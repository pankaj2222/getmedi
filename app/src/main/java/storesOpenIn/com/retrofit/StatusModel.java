package storesOpenIn.com.retrofit;
import java.util.List;

/**
 * Created by pankaj on 7/24/2017.
 */
public class StatusModel {
    String status;
    String message;
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<Response> response;
    public List<Response> getResponse() {
        return response;
    }
    public void setResponse(List<Response> response) {
        this.response = response;
    }
    public class Response {
        String message;
        String status;
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public String getStatus() {
            return status;
        }
        public void setStatus(String status) {
            this.status = status;
        }
    }
}
