package storesOpenIn.com.retrofit;



import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import storesOpenIn.com.onboarding.model.OtpModel;

public interface InterfaceApi {
    /* @GET url is after Base_URL.
       We are going to get List of country as response.
    */
    /*@GET("/~esambal/api/get_school/{news_cat_id}")
     Call<List<DataModel>> getCountries(@Path("news_cat_id") String id);*/
    @GET("/v1/articles")
    Call<DataModel> getCountries(@Query("source") String source,
                                 @Query("latest") String latest,
                                 @Query("apiKey") String apiKey);





    @POST("/pharma/admin/WEBSERVICES/sendotpcode")
    @FormUrlEncoded
    Call<OtpModel> sendOtp(@Field("APIKEY") String APIKEY,
                           @Field("mobile") String mobile
    );



    @POST("/pharma/admin/WEBSERVICES/verify_otp")
    @FormUrlEncoded
    Call<StatusModel> checkOTP(@Field("APIKEY") String APIKEY,
                               @Field("otpcode") String otpcode,
                               @Field("mobile") String mobile

    );



    @Multipart
    @POST("/pharma/admin/WEBSERVICES/upload_files")
    Call<StatusModel> uploadImage(@Part MultipartBody.Part[] image_name,
                                  @Part("mobile") String mobile,
                                  @Part("order") String order,
                                  @Part("APIKEY") String APIKEY);


    @Multipart
    @POST("/pharma/admin/WEBSERVICES/upload_files")
    Call<ResponseBody> uploadPriscrtionImage(@Part MultipartBody.Part[] mediImage,
                                           @Part("APIKEY") String APIKEY,
                                           @Part("mobile") RequestBody mobile,
                                           @Part("order") RequestBody order

    );


}