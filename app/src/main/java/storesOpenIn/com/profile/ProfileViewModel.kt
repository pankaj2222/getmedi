package storesOpenIn.com.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import storesOpenIn.com.addVolunteer.data.UserVolunteer
import storesOpenIn.com.utils.*

/**
 * Created by BalaKrishnan
 */
data class ProfileVMData(val preferenceHelper: PreferenceHelper)
class ProfileViewModel(profileVMData: ProfileVMData) : ViewModel() {
    private val _navigationWithParam = MutableLiveData<Event<Navigation>>()
    val navigationWithParamEvent: LiveData<Event<Navigation>> = _navigationWithParam
    val token =
        "Bearer " + profileVMData.preferenceHelper.user_token_id
    val gson = GsonBuilder().create()
    var userId: String = ""
    private val TAG = javaClass.name

    init {
        FirebaseAuth.getInstance().currentUser?.uid?.let {
            Log.d(TAG, "${it} ")
            userId = it
        }

//        viewModelScope.launch {
//            val string =
//                RetrofitUtil.getGraphQLAPI().postValue(value = token, requestBody = userDataQuery())
//            val fromJson = gson.fromJson(string.string(), UserVolunteer::class.java)
//
//            val users = fromJson?.data?.users
//            if (users != null && users.isNotEmpty())
//                users[0].auth_id.let {
//                    userId = it
//                }
//        }
    }
}

private fun userDataQuery(): RequestBody =
    "{\"query\":\"query MyQuery {\\n  users {\\n    auth_id\\n  }\\n}\\n\",\"variables\":{}}".getRequestBody()
