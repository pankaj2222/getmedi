package storesOpenIn.com.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_profile.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.AddVolunteerActivity
import storesOpenIn.com.onboarding.LauncherActivity
import storesOpenIn.com.storeListing.StoreListingActivity
import storesOpenIn.com.utils.*

class ProfileActivity : AppCompatActivity() {

    private val profileViewModel by lazy {
        ViewModelFactory(ProfileVMData(PreferenceHelper(this))).create(
            ProfileViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        profileViewModel.token
        (application as BaseApplication).isVolunteer.observe(this, Observer {
            if (it == null) return@Observer
            tvPIVolunteers.isEnabled = it
            tvPDVolunteers.isEnabled = it
            flVolunteersContainer.isEnabled = it
            val alpha = if(it) 1.0f else 0.5f
            tvPIVolunteers.alpha =alpha
            tvPDVolunteers.alpha = alpha
            flVolunteersContainer.alpha = alpha

        })
    }

    fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.actionBack -> onBackPressed()
                R.id.tvPIStore, R.id.tvPDStore, R.id.flStoresContainer -> {
                   /* val intent = Intent(this, StoreListingActivity::class.java)
                    intent.putExtra(KEY_NAME, "My Stores")
                    intent.putExtra(KEY_FROM_PROFILE, true)
                    intent.putExtra(KEY_ICON_OUTLINE, R.drawable.ic_grocery_outline)
                    intent.putExtra(KEY_USER_ID, profileViewModel.userId)
                    intent.putExtra(KEY_ICON, R.drawable.ic_grocery)
                    startActivity(intent)*/
                    showToast("On Progress...")
                }
                R.id.tvPIVolunteers, R.id.tvPDVolunteers, R.id.flVolunteersContainer -> {
                    /*val intent = Intent(this, AddVolunteerActivity::class.java)
                    intent.putExtra(KEY_FROM_PROFILE, true)
                    startActivity(intent)*/
                    showToast("On Progress...")
                }
                R.id.tvPIAccountSettings, R.id.tvPDAccountSettings, R.id.flAccountContainer -> {
                    showToast("Account Click")
                }
                R.id.tvPILogout, R.id.flLogoutContainer -> {
                    FirebaseAuth.getInstance().signOut()
                    val preferenceHelper = PreferenceHelper(this)
                    preferenceHelper.user_name = ""
                    preferenceHelper.user_email = ""
                    preferenceHelper.user_phone_number = ""
                    preferenceHelper.user_token_id = ""
                    preferenceHelper.user_logged_in = false
                    startActivity(Intent(this, LauncherActivity::class.java))
                    finish()
                }
            }
        }
    }
}
