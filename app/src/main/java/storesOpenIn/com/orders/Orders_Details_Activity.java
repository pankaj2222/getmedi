package storesOpenIn.com.orders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import storesOpenIn.com.R;
import storesOpenIn.com.utils.FileCompressor;

public class Orders_Details_Activity extends AppCompatActivity implements OrderListDetailsRecyclerViewAdapter.ItemClickListener {
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;
    File mPhotoFile;
    FileCompressor mCompressor;
    String currentPhotoPath;
    @BindView(R.id.rv_order_details)
    RecyclerView rv_order_list;
    OrderListDetailsRecyclerViewAdapter adapter;
    ArrayList<String> animalNames = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_details_layout);
        ButterKnife.bind(this);
        animalNames.add("Rs. 35");
        animalNames.add("RS. 40");
        animalNames.add("RS. 40");
        animalNames.add("RS. 05");
        animalNames.add("RS. 45");
        animalNames.add("RS. 30");
        animalNames.add("RS. 10");
        animalNames.add("RS. 80");
        animalNames.add("RS. 60");
        animalNames.add("Rs. 35");
        animalNames.add("RS. 40");
        animalNames.add("RS. 40");
        animalNames.add("RS. 05");
        animalNames.add("RS. 45");
        animalNames.add("RS. 30");
        animalNames.add("RS. 10");
        animalNames.add("RS. 80");
        animalNames.add("RS. 60");

        rv_order_list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new OrderListDetailsRecyclerViewAdapter(this, animalNames);
        rv_order_list.setAdapter(adapter);
        adapter.setClickListener(this);

    }



    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();

    }
}// end class
