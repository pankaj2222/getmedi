package storesOpenIn.com.orders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import storesOpenIn.com.BuildConfig;
import storesOpenIn.com.R;
import storesOpenIn.com.camera.adapter.ImageRecyclerViewAdapter;
import storesOpenIn.com.utils.FileCompressor;

public class Orders_List_Activity extends AppCompatActivity implements OrderListRecyclerViewAdapter.ItemClickListener {
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;
    File mPhotoFile;
    FileCompressor mCompressor;
    String currentPhotoPath;
    @BindView(R.id.rv_order_list)
    RecyclerView rv_order_list;
    OrderListRecyclerViewAdapter adapter;
    ArrayList<String> animalNames = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_list_layout);
        ButterKnife.bind(this);
        animalNames.add("14/06/2020");
        animalNames.add("10/06/2020");
        animalNames.add("09/06/2020");
        animalNames.add("05/06/2020");
        animalNames.add("01/05/2020");
        animalNames.add("14/06/2020");
        animalNames.add("10/06/2020");
        animalNames.add("09/06/2020");
        animalNames.add("05/06/2020");
        animalNames.add("01/05/2020");
        rv_order_list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new OrderListRecyclerViewAdapter(this, animalNames);
        rv_order_list.setAdapter(adapter);
        adapter.setClickListener(this);

    }



    @Override
    public void onItemClick(View view, int position) {
       // Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(Orders_List_Activity.this, Orders_Details_Activity.class));

    }
}// end class
