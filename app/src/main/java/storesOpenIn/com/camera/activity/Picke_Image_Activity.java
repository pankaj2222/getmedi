package storesOpenIn.com.camera.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import storesOpenIn.com.BuildConfig;
import storesOpenIn.com.R;
import storesOpenIn.com.camera.adapter.ImageRecyclerViewAdapter;
import storesOpenIn.com.onboarding.model.OtpModel;
import storesOpenIn.com.orders.Orders_List_Activity;
import storesOpenIn.com.retrofit.ApiClient;
import storesOpenIn.com.retrofit.InterfaceApi;
import storesOpenIn.com.retrofit.StatusModel;
import storesOpenIn.com.utils.FileCompressor;

public class Picke_Image_Activity extends AppCompatActivity implements ImageRecyclerViewAdapter.ItemClickListener {
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;
    File mPhotoFile;
    FileCompressor mCompressor;
    String currentPhotoPath;
    @BindView(R.id.rv_seleted_img)
    RecyclerView rv_selected_img;
    @BindView(R.id.btn_proceed)
    Button btn_proceed;
    @BindView(R.id.img_pic)
     ImageView img_tips;
    @BindView(R.id.txt_tips)
    TextView text_tips;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.progressBar4)
    ContentLoadingProgressBar progressBar;
    ImageRecyclerViewAdapter adapter;
    ArrayList<String> imagePath = new ArrayList<>();
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picke__image_);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
/*
        animalNames.add("Horse");
        animalNames.add("Cow");
        animalNames.add("Camel");
        animalNames.add("Sheep");
        animalNames.add("Goat");*/
        rv_selected_img.setLayoutManager(new GridLayoutManager(this,3));

        adapter = new ImageRecyclerViewAdapter(this, imagePath);
        rv_selected_img.setAdapter(adapter);
        adapter.setClickListener(this);

    }



    @OnClick(R.id.txt_camera)
    public void onCameraCall(){
        requestStoragePermission(true);

    }

    @OnClick(R.id.txt_gallery)
    public void ongalleryCal(){
        requestStoragePermission(false);
    }

    @OnClick(R.id.btn_proceed)
    public void onProceed(){


        uploadImage();
        //showAlertDialog(R.layout.dialog_positive_layout);
    }

    private void uploadImage() {
        progressBar.show();
        final RequestBody APIKEY = RequestBody.create(MediaType.parse("text/plain"), ApiClient.Apikey);
        final RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), "7000905871");
        final RequestBody order = RequestBody.create(MediaType.parse("text/plain"), "0");
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imagePath.size()];

        for (int i = 0; i < imagePath.size(); i++) {
            File file = new File(imagePath.get(i));
            final RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            surveyImagesParts[i] = MultipartBody.Part.createFormData("image_name[]", file.getName(), reqFile);
        }

        InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
        Call<ResponseBody> call = api.uploadPriscrtionImage(surveyImagesParts,APIKEY.toString(), mobile, order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
              //  Toast.makeText(Picke_Image_Activity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                progressBar.hide();
                showAlertDialog(R.layout.dialog_positive_layout);
                //finish();
               /* ResponseBody uhidModel = response.body();

                 try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                System.out.println("testingsassasa" + jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    private void showAlertDialog(int layout){
        dialogBuilder = new AlertDialog.Builder(Picke_Image_Activity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                imagePath.clear();
                showViewVisibility();
                startActivity(new Intent(Picke_Image_Activity.this, Orders_List_Activity.class));
            }
        });
    }
    /**
     * Requesting multiple permissions (storage and camera) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void dispatchTakePictureIntent() { Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                mPhotoFile = photoFile;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }


    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                seToList(getImagePath());
                //Glide.with(Picke_Image_Activity.this).load(mPhotoFile).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_medicine_outline)).into(imageViewProfilePic);
                //imageViewProfilePic.setImageBitmap(imageBitmap);
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                mPhotoFile = new File(getRealPathFromUri(selectedImage));
                seToList(getRealPathFromUri(selectedImage));
              //  imagePath.add(getRealPathFromUri(selectedImage));
               // Glide.with(Picke_Image_Activity.this).load(mPhotoFile).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_medicine_outline)).into(imageViewProfilePic);

            }
        }
    }

    private void seToList(String selectedImage) {
        imagePath.add(selectedImage);
        adapter.notifyDataSetChanged();
        showViewVisibility();
    }

    private void showViewVisibility() {
        if (imagePath.isEmpty()){
            rv_selected_img.setVisibility(View.GONE);
            btn_proceed.setVisibility(View.GONE);
            img_tips.setVisibility(View.VISIBLE);
            text_tips.setText("Valid prescription tips");
            radioGroup.setVisibility(View.GONE);
        }else {
            rv_selected_img.setVisibility(View.VISIBLE);
            btn_proceed.setVisibility(View.VISIBLE);
            img_tips.setVisibility(View.GONE);
            text_tips.setText("Continue to upload ...");
            radioGroup.setVisibility(View.VISIBLE);

        }
    }

    public String getImagePath() {
        return currentPhotoPath;
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    /**
     * Create file with current timestamp name
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @Override
    public void onItemClick(View view, int position) {
        //Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
        imagePath.remove(position);
        adapter.notifyItemRemoved(position);
        showViewVisibility();
    }
}// end class
