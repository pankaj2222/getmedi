package storesOpenIn.com

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.storeListing.Store
import storesOpenIn.com.utils.ConnectionStateMonitor
import storesOpenIn.com.utils.PreferenceHelper

/**
 * Created by BalaKrishnan
 */
class BaseApplication : Application() {
    var location: LatLong? = null
    private val TAG = javaClass.name
    val isVolunteer = MutableLiveData<Boolean>()
    val storeHelpful = MutableLiveData<Store?>()

    override fun onCreate() {
        super.onCreate()
        val pref = PreferenceHelper(this)

        FirebaseAuth.getInstance().addIdTokenListener(
            FirebaseAuth.IdTokenListener {

                it.currentUser?.getIdToken(false)?.addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        Log.d(TAG, "FBToken : ${task.result?.token} ")
                        val idToken = task.result!!.token
                        if (idToken != null) pref.user_token_id =
                            idToken else pref.user_token_id =
                            "" //Token ID is not available
                    }
                }
            })

    }
}