package storesOpenIn.com.storeDetails

import android.content.Intent
import androidx.annotation.Keep
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import storesOpenIn.com.storeListing.Response
import storesOpenIn.com.storeListing.Store
import storesOpenIn.com.utils.*


/**
 * Created by BalaKrishnan
 */
data class StoreDetailsVMData(
    val intent: Intent,
    val preferenceHelper: PreferenceHelper
)


class StoreDetailsViewModel(
    storeDetailsVMData: StoreDetailsVMData
) :
    ViewModel() {
    private var coordinates: List<Double>? = null

    private val _navigationWithParam = MutableLiveData<Event<Navigation>>()
    val navigationWithParamEvent: LiveData<Event<Navigation>> = _navigationWithParam
    var id: Int? = null

    val store: Store? = getStoreFromIntent(storeDetailsVMData.intent)

    private fun getStoreFromIntent(intent: Intent): Store? {
        val appLinkData = intent.data
        return if (appLinkData == null) {
            intent.getParcelableExtra(StoreDetailsActivity.KEY_STORE_VALUE) as Store
        } else {
            Store(id = appLinkData.getQueryParameter("storeId")!!.toInt())
        }

    }

    val token =
        "Bearer " + storeDetailsVMData.preferenceHelper.user_token_id

    init {
        viewModelScope.launch {
            id = store?.id
            val response = RetrofitUtil.getGraphQLAPI()
                .postValue(value = token, requestBody = getStoreDetailsQuery(id))
            val string = response.string()
            if (!string.isNullOrEmpty()) {
                val fromJson = string.fromJson(Response::class.java)
                fromJson?.let {
                    if (it.data.stores.isNotEmpty()) {
                        coordinates = it.data.stores[0].locationByLocation.location.coordinates
                        _navigationWithParam.postValue(
                            Event(
                                Navigation(
                                    Action.RECEIVED_STORE_DETAILS,
                                    it.data.stores[0]
                                )
                            )
                        )
                    }
                }
            }
        }

    }

    fun requestStoreHelpful(isHelpful: Boolean?) {
        viewModelScope.launch {
            id = store?.id
            val response = RetrofitUtil.getGraphQLAPI()
                .postValue(value = token, requestBody = getStoreHelpfullQuery(id, isHelpful))


            val postValue = RetrofitUtil.getGraphQLAPI()
                .postValue(value = token, requestBody = getHelpCountQuery())
            val data = postValue.string().fromJson(StoreHelpfulData::class.java)
            _navigationWithParam.postValue(Event(Navigation(Action.HELP_DATA, data)))

        }
    }

    @Keep
    data class StoreHelpfulData(
        @SerializedName("data")
        val `data`: Data = Data()
    )

    @Keep
    data class Data(
        @SerializedName("no_of_helpful")
        val noOfHelpful: NoOfHelpful = NoOfHelpful(),
        @SerializedName("no_of_not_helpful")
        val noOfNotHelpful: NoOfNotHelpful = NoOfNotHelpful()
    )

    @Keep
    data class NoOfHelpful(
        @SerializedName("aggregate")
        val aggregate: Aggregate = Aggregate()
    )

    @Keep
    data class Aggregate(
        @SerializedName("count")
        val count: Int = 0 // 2
    )

    @Keep
    data class NoOfNotHelpful(
        @SerializedName("aggregate")
        val aggregate: AggregateX = AggregateX()
    )

    @Keep
    data class AggregateX(
        @SerializedName("count")
        val count: Int = 0 // 1
    )

    fun getStoreDetailsQuery(storeID: Int?): RequestBody {
        val payload =
            "{\n" +
                    "  \"query\": \"query storeDetails(\$authID: String, \$id: Int!) {\\n  stores(where: {id: {_eq: \$id}}) {\\n    id\\n    name\\n    description\\n    store_x_categories {\\n      category {\\n        id\\n        name\\n      }\\n    }\\n    user {\\n      id\\n      name\\n    }\\n    not_helpful: helpfuls_aggregate(where: {is_helpful: {_eq: false}}) {\\n      aggregate {\\n        count\\n      }\\n    }\\n    helpful: helpfuls_aggregate(where: {is_helpful: {_eq: true}}) {\\n      aggregate {\\n        count\\n      }\\n    }\\n    helpfuls(where: {user_id: {_eq: \$authID}}) {\\n      is_helpful\\n    }\\n    locationByLocation {\\n      location\\n    }\\n  }\\n}\\n\",\n" +
                    "  \"variables\": {\n" +
                    "    \"id\": $storeID,\n" +
                    "    \"authID\": \"${FirebaseAuth.getInstance().uid}\"\n" +
                    "  }\n" +
                    "}"
        return payload.getRequestBody()
    }

    fun getStoreHelpfullQuery(storeID: Int?, isHelpful: Boolean?): RequestBody {
        val s = "{\n" +
                "  \"query\": \"mutation mutateHelpful(\$store_id:Int!, \$is_helpful:Boolean!) {\\n  insert_helpful(objects: {store_id: \$store_id, is_helpful: \$is_helpful}, \\n    on_conflict: {constraint: helpful_pkey, update_columns: is_helpful}\\n  ) {\\n    affected_rows\\n  }\\n}\",\n" +
                "  \"variables\": {\n" +
                "    \"store_id\": $storeID,\n" +
                "    \"is_helpful\": $isHelpful\n" +
                "  }\n" +
                "}"
        return s.getRequestBody()
    }

    fun getHelpCountQuery(storeID: Int? = id): RequestBody {
        return "{\"query\":\"query getHelpfulAggregateForStore(\$store_id:Int!) {\\n  no_of_helpful: helpful_aggregate(where: {is_helpful: {_eq: true}, _and: {store: {id: {_eq: \$store_id}}}}) {\\n    aggregate {\\n      count\\n    }\\n  }\\n  no_of_not_helpful: helpful_aggregate(where: {is_helpful: {_eq: false}, _and: {store: {id: {_eq: \$store_id}}}}) {\\n    aggregate {\\n      count\\n    }\\n  }\\n}\",\"variables\":{\"store_id\":$storeID}}".getRequestBody()
    }

    fun getLocation(): List<Double>? {
        return coordinates
    }


}