package storesOpenIn.com.storeDetails

import android.R.attr.label
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.shape.ShapeAppearanceModel
import kotlinx.android.synthetic.main.activity_store_details.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addStore.AddStoreActivity
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.storeListing.*
import storesOpenIn.com.utils.*


class StoreDetailsActivity : AppCompatActivity() {
    lateinit var viewModel: StoreDetailsViewModel
    private val TAG = javaClass.name
    private var currentLatLong: LatLong = LatLong()
    private var storeLatLong: LatLong = LatLong()

    private val liveLocationHelper: LiveLocationHelper by lazy {
        LiveLocationHelper(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        liveLocationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    val clrWhite: Int by lazy { ContextCompat.getColor(this, android.R.color.white) }
    val clrMildBlack: Int by lazy { ContextCompat.getColor(this, R.color.colorTextPrimary) }
    val clrRed: Int by lazy { ContextCompat.getColor(this, R.color.notHelpfulColor) }
    val clrGreen: Int by lazy { ContextCompat.getColor(this, R.color.colorAccent) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_details)

        executionOnInternet {
            viewModel = ViewModelFactory(StoreDetailsVMData(intent, PreferenceHelper(this))).create(
                StoreDetailsViewModel::class.java
            )
            setupNavigationEventListener()
        }

        liveLocationHelper.observe(this, Observer {
            currentLatLong = it
            distanceAway()
        })


        setupUI()

    }

    private fun setupNavigationEventListener() {
        viewModel.navigationWithParamEvent.observe(this, EventObserver {
            when (it.action) {
                Action.RECEIVED_STORE_DETAILS -> {
                    val param = it.param as Store
                    storeTitle.text = param.name

                    Log.d(TAG, " store details ${param.toString()} ")

                    param.storeXCategories.forEach { it ->
                        Log.d(TAG, " adding chip ${it.category.name} ")
                        addchip(cgCategory, it.category.name)
                    }

                    labelOrder.text = param.helpful.aggregate.count.toString() + " users"
                    labelDate.text = param.notHelpful.aggregate.count.toString() + " users"
                    labelUsername.text = param.user?.name
                    labelStoreDescription.text = param.description
                    if (param.helpfuls.isNotEmpty()) {
                        param.helpfuls[0].isHelpful?.let { isHelpful ->
                            if (isHelpful) {
                                onBtnClick(false, actionHelpful, actionNotHelpful)
                            } else {
                                onBtnClick(true, actionNotHelpful, actionHelpful)
                            }
                        }
                    }
                    storeLatLong.lat = param.locationByLocation.location.coordinates.get(0)
                    storeLatLong.lng = param.locationByLocation.location.coordinates.get(1)
                    distanceAway()

                }
                Action.HELP_DATA -> {
                    val param = it.param as StoreDetailsViewModel.StoreHelpfulData
                    labelOrder.text = param.data.noOfHelpful.aggregate.count.toString() + " users"
                    labelDate.text =
                        param.data.noOfNotHelpful.aggregate.count.toString() + " users"
                    val store = Store(
                        id = viewModel.id!!,
                        helpful = Helpful(Aggregate(count = param.data.noOfHelpful.aggregate.count)),
                        notHelpful = NotHelpful(AggregateX(count = param.data.noOfNotHelpful.aggregate.count))
                    )
                    (application as BaseApplication).storeHelpful.postValue(store)
                }
            }
        })
    }

    private fun setupUI() {
        actionBack.setOnClickListener { onBackPressed() }
        actionShare.setOnClickListener {
            shareApp(getString(R.string.storeShare) + viewModel.id)
        }
        var fromProfile = false

        intent?.let {
            fromProfile = it.getBooleanExtra(KEY_FROM_PROFILE, false)
        }

        helpfulHolder.setVisible(!fromProfile)
        actionEdit.setVisible(fromProfile)
        actionEdit.setOnClickListener {
            val intent = Intent(this@StoreDetailsActivity, AddStoreActivity::class.java)
            intent.apply {
                putExtra(KEY_FROM_PROFILE, true)
                putExtra(KEY_STORE_DETAILS, viewModel.store)
            }
            startActivity(intent)
        }


        actionStartNavigation.setOnClickListener {
            val location = viewModel.getLocation()
            try {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "geo:" + location?.get(0)
                            .toString() + "," + location?.get(1)
                            .toString() + "?q=" + location?.get(0)
                            .toString() + "," + location?.get(1)
                            .toString() + "(" + label.toString() + ")"
                    )
                )
                intent.component = ComponentName(
                    "com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity"
                )
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=com.google.android.apps.maps")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.maps")
                        )
                    )
                }
                e.printStackTrace()
            }
        }

        actionNotHelpful.setOnClickListener {
            viewModel.requestStoreHelpful(false)
            onBtnClick(true, actionNotHelpful, actionHelpful)
        }
        actionHelpful.setOnClickListener {
            viewModel.requestStoreHelpful(true)
            onBtnClick(false, actionHelpful, actionNotHelpful)
        }
    }

    private fun onBtnClick(type: Boolean, btn1: MaterialButton, btn2: MaterialButton) {
        btn1.setIconTintResource(R.color.white)
        btn1.setBackgroundColor(if (type) clrRed else clrGreen)
        btn1.setTextColor(clrWhite)
        btn1.strokeWidth = 0

        btn2.strokeWidth = resources.getDimension(R.dimen.mtrl_btn_stroke_size).toInt()
        btn2.setBackgroundColor(clrWhite)
        btn2.setTextColor(clrMildBlack)
        btn2.setIconTintResource(R.color.colorTextPrimary)
    }

    private fun distanceAway() {
        if (currentLatLong.isEmpty() || storeLatLong.isEmpty()) return
        val currentLocation = Location("locationA")
        currentLocation.latitude = currentLatLong.lat
        currentLocation.longitude = currentLatLong.lng

        val storeLocation = Location("locationB")
        storeLocation.latitude =
            storeLatLong.lat
        storeLocation.longitude =
            storeLatLong.lng
        Log.d(TAG, " distance ${currentLocation.distanceTo(storeLocation)}")
        tvDistace.text =
            (currentLocation.distanceTo(storeLocation).toInt() / 1000).toString() + " km away"
    }


    private fun addchip(chipGroup: ChipGroup?, tagName: String): Chip? {
        if (chipGroup != null && chipGroup.context != null) {
            val chip = Chip(chipGroup.context)
            chip.setText(tagName, TextView.BufferType.EDITABLE)
            chip.id = View.generateViewId()
            chip.shapeAppearanceModel =
                ShapeAppearanceModel.builder().setAllCornerSizes(20f).build()
            chip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            chip.checkedIcon = null
            chip.isCheckable = true
            chipGroup.addView(chip)
            return chip
        }
        return null

    }

    companion object {

        @kotlin.jvm.JvmField
        val KEY_STORE_VALUE = "StoreValue"
    }

}
