package storesOpenIn.com.dashboard

import androidx.annotation.Keep

@Keep
data class Item(
    val name: String,
    val resource: Int,
    val outlineResource: Int,
    val isHelpline: Boolean,
    val id: Int
)