package storesOpenIn.com.dashboard

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.location.*
import com.google.firebase.auth.FirebaseAuth
import com.schibstedspain.leku.LATITUDE
import com.schibstedspain.leku.LONGITUDE
import com.schibstedspain.leku.LocationPickerActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_dashboard.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addStore.AddStoreActivity
import storesOpenIn.com.addVolunteer.AddVolunteerActivity
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.camera.activity.Picke_Image_Activity
import storesOpenIn.com.onboarding.AboutUsActivity
import storesOpenIn.com.onboarding.PrivacyPolicyActivity
import storesOpenIn.com.onboarding.TOSActivity
import storesOpenIn.com.orders.Orders_List_Activity
import storesOpenIn.com.profile.ProfileActivity
import storesOpenIn.com.storeListing.StoreListingActivity
import storesOpenIn.com.utils.*
import storesOpenIn.com.utils.LocationDecode.decodeGeo
import storesOpenIn.com.volunteerListing.VolunteerListingActivity


class DashboardActivity : AppCompatActivity(),
    DashboardAdapter.OnItemClickListener {
    private val TAG = javaClass.name

    companion object {
        var mLastLocation: Location? = null
    }

    private val locationPermissionRequest = 123
    private val locationPickerRequest = 234
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private val connectionStateMonitor: ConnectionStateMonitor by lazy {
        ConnectionStateMonitor(this)
    }
    private lateinit var vm: DashboardViewModel
    val pref by lazy {
        PreferenceHelper(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        labelUsername.setText(pref.user_name)
        progressBar.show()
        addListeners()
        vm = ViewModelFactory(
            DashboardVMData(
                PreferenceHelper(this),
                intent,
                connectionStateMonitor = ConnectionStateMonitor(this)
            )
        ).create(DashboardViewModel::class.java)

        vm.init(this)
        bindObservers()
        getLastLocation()
        progressBar.hide()
       /* executionOnInternet {
            progressBar.show()
            FirebaseAuth.getInstance().currentUser!!.getIdToken(true)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        Log.d(TAG, "FBToken : ${task.result?.token} ")
                        val idToken = task.result!!.token
                        if (idToken != null) pref.user_token_id =
                            idToken else pref.user_token_id =
                            "" //Token ID is not available
                    }
                    labelUsername.setText(pref.user_name)
                    progressBar.show()
                    addListeners()
                    vm = ViewModelFactory(
                        DashboardVMData(
                            PreferenceHelper(this),
                            intent,
                            connectionStateMonitor = ConnectionStateMonitor(this)
                        )
                    ).create(DashboardViewModel::class.java)

                    vm.init(this)
                    bindObservers()
                    getLastLocation()
                    progressBar.hide()
                }
        }*/
    }

    fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.actionProfile -> {
                    executionRequireInternet {
                        startActivity(Intent(this, ProfileActivity::class.java))
                    }
                }
            }
        }
    }

    private fun bindObservers() {
        vm.items.observe(this, Observer {
            it?.let { items ->
                rvAroundYou.layoutManager = GridLayoutManager(this, 3)
                val adapter =
                    DashboardAdapter(items)
                adapter.addItemClickListener(this)
                rvAroundYou.adapter = adapter
            }
        })
        vm.user.observe(this, Observer {
            it?.let {
                (application as BaseApplication).isVolunteer.postValue(it.is_volunteer)
            }
        })
        (application as BaseApplication).isVolunteer.observe(this, Observer {
            it?.let {
                vm.isVolunteer = it
                if (it) {
                    actionAddVolunteer.alpha = 0.5f
                } else {
                    actionAddVolunteer.alpha = 1.0f
                }
            }
        })
    }


    private fun addListeners() {
        actionLanguage.setOnClickListener {

        }
        progressBar.hide()
        labelUsername.text = FirebaseAuth.getInstance().currentUser?.displayName

        labelChange.setOnClickListener {
            val locationPickerIntent = LocationPickerActivity.Builder()
                .withGeolocApiKey(getString(R.string.google_maps_key))
                .withVoiceSearchHidden()
                .withSearchZone("es_IN")
                .withGooglePlacesEnabled()
                .build(applicationContext)

            startActivityForResult(locationPickerIntent, locationPickerRequest)
        }

        actionAddStore.setOnClickListener {
            //showToast("Coming soon ....")
            val intent = Intent(this, Picke_Image_Activity::class.java)
            startActivity(intent)
        }

        actionAddVolunteer.setOnClickListener {
            showToast("Coming soon ....")
            executionRequireInternet {
                val intent = Intent(this, Orders_List_Activity::class.java)
                startActivity(intent)
            }
        }

        actionFindVolunteer.setOnClickListener {
            executionRequireInternet {
                val intent = Intent(this, Orders_List_Activity::class.java)
                startActivity(intent)
            }
        }

        actionShare.setOnClickListener {
            shareApp(getString(R.string.appShare))
        }

        btnPrivacy.setOnClickListener {
            val intent = Intent(this, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        aboutUsContainer.setOnClickListener {
          //  val intent = Intent(this, AboutUsActivity::class.java)
           // startActivity(intent)
        }

        btnTermsConditions.setOnClickListener {
            val intent = Intent(this, TOSActivity::class.java)
            startActivity(intent)
        }
        val uid = FirebaseAuth.getInstance().currentUser?.uid;
        Log.d(TAG, " uid $uid ")
    }

    override fun onItemClicked(item: Item) {
        executionRequireInternet {
            if (item.isHelpline) {
                Toast.makeText(this, "Covid support!", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, StoreListingActivity::class.java)
                intent.putExtra(KEY_NAME, item.name)
                intent.putExtra(KEY_ICON, item.resource)
                intent.putExtra(KEY_ICON_OUTLINE, item.outlineResource)
                intent.putExtra(KEY_CATEGORY_ID, item.id)
                startActivity(intent)
            }
        }
    }

    private fun getLastLocation() {
        if (checkLocationPermissions()) {
            if (isLocationEnabled()) {
                progressBar.show()
                if (mFusedLocationClient == null)
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                mFusedLocationClient?.lastLocation?.addOnCompleteListener { task ->
                    val location: Location? = task.result
                    location?.let {
                        onLocationReceived(location)
                    }
                }
            } else {
                val intent =
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun requestNewLocationData() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location? = locationResult.lastLocation
            onLocationReceived(location)
        }
    }

    private fun onLocationReceived(location: Location?) {
        location?.let { value ->
            progressBar.hide()
            val latLong = LatLong(value.latitude, value.longitude)
            labelUserLocation.text = decodeGeo(this@DashboardActivity, latLong)
            (application as BaseApplication).location =
                latLong
            Log.d(
                TAG,
                "onLocationReceived : ${(application as BaseApplication).location?.lat}, ${(application as BaseApplication).location?.lng} "
            )
        }
        if (location == null) {
            requestNewLocationData()
        } else {
            mLastLocation = location
        }
    }

    private fun checkLocationPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            locationPermissionRequest
        )
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            locationPermissionRequest -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        getLastLocation()
                    }
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage("Location access is required for browsing nearby stores.")
                        .setPositiveButton(
                            "OPEN SETTINGS"
                        ) { dialog, _ ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                            dialog.dismiss()
                        }
                    builder.create().show()
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == locationPickerRequest && resultCode == Activity.RESULT_OK && data != null) {
            val latitude = data.getDoubleExtra(LATITUDE, 0.0)
            val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
            (application as BaseApplication).location = LatLong(latitude, longitude)
            Log.d(TAG, "Location $latitude, $longitude ")
            labelUserLocation.text =
                decodeGeo(this@DashboardActivity, (application as BaseApplication).location!!)
        }
    }

}
