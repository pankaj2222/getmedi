package storesOpenIn.com.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import storesOpenIn.com.R
import kotlinx.android.synthetic.main.item_dashboard.view.*
import kotlinx.android.synthetic.main.item_dashboard_helpline.view.*

const val TYPE_ITEM = 101
const val TYPE_HELPLINE = 102

class DashboardAdapter(private val items: List<Item>) :
    RecyclerView.Adapter<DashboardAdapter.ViewHolder>() {

    private var listener: OnItemClickListener? = null
    fun addItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].isHelpline) TYPE_HELPLINE else TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val resource =
            if (viewType == TYPE_ITEM) R.layout.item_dashboard
            else R.layout.item_dashboard_helpline

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(resource, null, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: Item) {
            if (item.isHelpline) {
                itemView.iconViewHelp.setImageResource(item.resource)
                itemView.labelTitleHelp.text = item.name
            } else {
                itemView.iconView.setImageResource(item.resource)
                itemView.labelTitle.text = item.name
            }

            itemView.setOnClickListener { listener?.onItemClicked(item) }
        }
    }

    interface OnItemClickListener {
        fun onItemClicked(item: Item)
    }
}
