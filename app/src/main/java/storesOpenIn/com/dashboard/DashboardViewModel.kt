package storesOpenIn.com.dashboard

import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import kotlinx.coroutines.launch
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.data.User
import storesOpenIn.com.addVolunteer.data.UserVolunteer
import storesOpenIn.com.utils.*
import storesOpenIn.com.utils.APIQuery.updateUsername
import storesOpenIn.com.utils.RetrofitUtil.getGraphQLAPI

data class DashboardVMData(
    val preferenceHelper: PreferenceHelper,
    val intent: Intent,
    val connectionStateMonitor: ConnectionStateMonitor
)

class DashboardViewModel(val data: DashboardVMData) : ViewModel() {

    val items: MutableLiveData<List<Item>> = MutableLiveData()
    val user = MutableLiveData<User>(null)
    var isVolunteer = false
    var apiCallMade = false

    fun init(context: Context) {
        val itemsName = context.resources.getStringArray(R.array.items)
        val list: MutableList<Item> = getCategories(itemsName)
        items.postValue(list)
    }

    val gson = GsonBuilder().create()

    val token =
        "Bearer " + data.preferenceHelper.user_token_id

    init {
            userData()
            val graphQLAPI = getGraphQLAPI()
            FirebaseAuth.getInstance().currentUser?.displayName?.let {
                viewModelScope.launch {
                    graphQLAPI.postValue(
                        "", token, updateUsername(
                            it
                        )
                    )
                }
            }
    }

    fun userData() {
        viewModelScope.launch {
            val string = RetrofitUtil.getGraphQLAPI().postValue(
                value = token,
                requestBody = APIQuery.getUserDetails()
            ).string()
            val fromJson = gson.fromJson(string, UserVolunteer::class.java)
            fromJson?.data?.users?.let {
                if (it.isNotEmpty())
                    user.postValue(it[0])
            }

        }
    }
}