package storesOpenIn.com.onboarding

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.LinkConsumableTextView
import com.klinker.android.link_builder.applyLinks
import kotlinx.android.synthetic.main.activity_about_us.*
import storesOpenIn.com.R


class AboutUsActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        bindLinks()
        emailUs.setOnClickListener(this)
        emailUs1.setOnClickListener(this)
        emailUs2.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        joinDevGroup.setOnClickListener(this)
        joinOutreachGroup.setOnClickListener(this)
        pressKit.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.getId()) {
            R.id.emailUs -> {
                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:helpmate@netbramha.com")
                startActivity(Intent.createChooser(emailIntent, "Send feedback"))
            }
            R.id.emailUs1 -> {
                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:helpmate@netbramha.com")
                startActivity(Intent.createChooser(emailIntent, "Send feedback"))
            }
            R.id.emailUs2 -> {
                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:helpmate@netbramha.com")
                startActivity(Intent.createChooser(emailIntent, "Send feedback"))
            }

            R.id.ivBack -> {
                onBackPressed()
            }

            R.id.joinDevGroup -> {
                val intentWhatsapp = Intent(Intent.ACTION_VIEW)
                val url = "https://chat.whatsapp.com/BEfx54RzLMp6hXVA0GaqhR"
                intentWhatsapp.data = Uri.parse(url)
                intentWhatsapp.setPackage("com.whatsapp")
                startActivity(intentWhatsapp)
            }

            R.id.joinOutreachGroup -> {
                val intentWhatsapp = Intent(Intent.ACTION_VIEW)
                val url = "https://chat.whatsapp.com/DwxJ5K6W2C6IL1F09pR5nh"
                intentWhatsapp.data = Uri.parse(url)
                intentWhatsapp.setPackage("com.whatsapp")
                startActivity(intentWhatsapp)
            }

            R.id.pressKit -> {
                val i = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://docs.google.com/document/d/1sywfi8zieOFmX4QLcjcEA9WQ8-jtc2s5xfcdyecv1Vs/edit?usp=sharing")
                )
                startActivity(i)
            }
        }
    }

    private fun bindLinks() {
        val dev1 = findViewById<View>(R.id.tv_about_dev1) as LinkConsumableTextView
        val dev2 = findViewById<View>(R.id.tv_about_dev2) as LinkConsumableTextView
        val dev3 = findViewById<View>(R.id.tv_about_dev3) as LinkConsumableTextView
        val dev4 = findViewById<View>(R.id.tv_about_dev4) as LinkConsumableTextView
        val tvWebsite = findViewById<View>(R.id.tv_website) as LinkConsumableTextView
        val color = ContextCompat.getColor(this, R.color.textColorBold)
        val dev1Link = Link(dev1.text.toString())
            .setTypeface(Typeface.DEFAULT_BOLD)
            .setTextColorOfHighlightedLink(color)
            .setTextColor(color)
            .setOnClickListener {
                openLink("https://twitter.com/i_shankar")
            }


        val dev2Link = Link(dev2.text.toString())
            .setTypeface(Typeface.DEFAULT_BOLD)
            .setTextColorOfHighlightedLink(color)
            .setTextColor(color)
            .setOnClickListener {
                openLink("https://twitter.com/goonahDroid")
            }

        val dev3Link = Link(dev3.text.toString())
            .setTypeface(Typeface.DEFAULT_BOLD)
            .setTextColorOfHighlightedLink(color)
            .setTextColor(color)
            .setOnClickListener {
                openLink("https://twitter.com/BalakrishnanPT")
            }

        val dev4Link = Link(dev4.text.toString())
            .setTypeface(Typeface.DEFAULT_BOLD)
            .setTextColorOfHighlightedLink(color)
            .setTextColor(color)
            .setOnClickListener {
                openLink("https://twitter.com/NorthJaw")
            }
        val website = Link(tvWebsite.text.toString())
            .setTypeface(Typeface.DEFAULT_BOLD)
            .setTextColorOfHighlightedLink(color)
            .setTextColor(color)
            .setOnClickListener {
                openLink("https://www.netbramha.com")
            }
        dev1.applyLinks(dev1Link)
        dev2.applyLinks(dev2Link)
        dev3.applyLinks(dev3Link)
        dev4.applyLinks(dev4Link)
        tvWebsite.applyLinks(website)
    }

    private fun openLink(twitterAddressLink: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(twitterAddressLink))
        startActivity(browserIntent)
    }
}