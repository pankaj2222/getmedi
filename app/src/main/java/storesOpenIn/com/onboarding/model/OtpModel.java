package storesOpenIn.com.onboarding.model;
import java.util.List;

/**
 * Created by pankaj on 7/24/2017.
 */
public class OtpModel {
    String status;
    String message;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public List<Response> response;
    public List<Response> getResponse() {
        return response;
    }
    public void setResponse(List<Response> response) {
        this.response = response;
    }
    public class Response {
        String otp_code;
        String mobile;

        public String getOtp_code() {
            return otp_code;
        }

        public void setOtp_code(String otp_code) {
            this.otp_code = otp_code;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
