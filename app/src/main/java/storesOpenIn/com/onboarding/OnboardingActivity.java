package storesOpenIn.com.onboarding;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import storesOpenIn.com.R;
import storesOpenIn.com.onboarding.RegisterUserBottomDialogFragment;
import com.google.firebase.FirebaseApp;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnboardingActivity extends AppCompatActivity {

    private RegisterUserBottomDialogFragment bottomDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_onboarding3);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_get_started)
    public void onGetStarted(){
        bottomDialogFragment = RegisterUserBottomDialogFragment.newInstance();
        bottomDialogFragment.setCancelable(false);
        bottomDialogFragment.show(getSupportFragmentManager(),RegisterUserBottomDialogFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (bottomDialogFragment.isVisible())  bottomDialogFragment.dismiss();
    }
}
