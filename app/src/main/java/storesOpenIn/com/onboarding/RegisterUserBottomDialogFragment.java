package storesOpenIn.com.onboarding;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.saket.bettermovementmethod.BetterLinkMovementMethod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import storesOpenIn.com.R;
import storesOpenIn.com.dashboard.DashboardActivity;
import storesOpenIn.com.onboarding.model.OtpModel;
import storesOpenIn.com.retrofit.ApiClient;
import storesOpenIn.com.retrofit.InterfaceApi;
import storesOpenIn.com.retrofit.StatusModel;
import storesOpenIn.com.utils.CommonExtensionsKt;
import storesOpenIn.com.utils.PreferenceHelper;
import storesOpenIn.com.widgets.otpview.OtpView;

import static android.app.Activity.RESULT_OK;

public class RegisterUserBottomDialogFragment extends BottomSheetDialogFragment implements TextWatcher, OtpReceivedInterface, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    static final String TAG = "RegisterUserBottomDialog";
    private static final int RESOLVE_HINT = 1;
    @BindView(R.id.register_user)
    ConstraintLayout registerUserContainer;
    @BindView(R.id.register_mobile)
    ConstraintLayout registerMobileContainer;
    @BindView(R.id.register_otp)
    ConstraintLayout registerOTPContainer;
    @BindView(R.id.register_name)
    ConstraintLayout registerNameContainer;
    @BindView(R.id.register_email)
    ConstraintLayout registerEmailContainer;
    @BindView(R.id.register_password)
    ConstraintLayout registerPasswordContainer;
    @BindView(R.id.login_user)
    ConstraintLayout loginUserContainer;
    @BindView(R.id.country_code_picker)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.editText_carrierNumber)
    EditText etCarrierNumber;
    @BindView(R.id.editText_login_password)
    EditText etLoginPassword;
    @BindView(R.id.text_input_layout_container)
    TextInputLayout passwordInputLayoutContainer;
    @BindView(R.id.editText_name)
    EditText etUserName;
    @BindView(R.id.editText_mail)
    EditText etUserEmail;
    @BindView(R.id.editText_password)
    EditText etUserPassword;
    @BindView(R.id.editText_confirm_password)
    EditText etUserConfirmPassword;
    @BindView(R.id.tv_header_otp)
    TextView tvHeaderOTP;
    @BindView(R.id.tv_user_mail)
    TextView tvUserEmail;
    @BindView(R.id.tv_login_mail)
    TextView tvLoginEmail;
    @BindView(R.id.register_otp_view)
    OtpView registerOtpView;
    @BindView(R.id.btn_send_otp)
    Button btnSendOtp;
    @BindView(R.id.btn_verify_otp)
    Button btnVerifyOtp;
    @BindView(R.id.btn_register_continue)
    Button btnRegisterContinue;
    @BindView(R.id.btn_register_email_next)
    Button btnRegisterEmailNxt;
    @BindView(R.id.btn_register_password_next)
    Button btnRegisterPasswordNxt;
    @BindView(R.id.tv_password_match_txt)
    TextView tvPasswordMatchTxt;
    @BindView(R.id.tv_resend_counter)
    TextView tvResendCounterView;
    @BindView(R.id.tv_terms)
    TextView tvTermsConditionsTxt;
    private Activity activity;
    String mobile_no="";
    GoogleApiClient googleApiClient;
    private final BetterLinkMovementMethod.OnLinkClickListener urlClickListener = (view, url) -> {
        if (url.contains("https://tos")) {
            startActivity(new Intent(activity, TOSActivity.class));
        } else startActivity(new Intent(activity, PrivacyPolicyActivity.class));
        return true;
    };
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private FirebaseUser firebaseUser;
    private PreferenceHelper preferenceHelper;
    private CountDownTimer otpCountdownTimer;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    public MySMSBroadcastReceiver mySMSBroadcastReceiver;

    static RegisterUserBottomDialogFragment newInstance() {
        return new RegisterUserBottomDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        preferenceHelper = new PreferenceHelper(activity);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_register_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        countryCodePicker.registerCarrierNumberEditText(etCarrierNumber);
        registerOtpView.setOtpCompletionListener(otp -> CommonExtensionsKt.hideKeyBoard(activity, registerOtpView));
        etCarrierNumber.addTextChangedListener(this);
        etUserName.addTextChangedListener(this);
        etUserEmail.addTextChangedListener(this);
        etUserPassword.addTextChangedListener(this);
        etUserConfirmPassword.addTextChangedListener(this);
        registerOtpView.addTextChangedListener(this);
        tvTermsConditionsTxt.setText(Html.fromHtml(getString(R.string.content_onboarding_terms_txt)));
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        mySMSBroadcastReceiver.setOnOtpListeners(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        activity.registerReceiver(mySMSBroadcastReceiver, intentFilter);
        BetterLinkMovementMethod.linkifyHtml(tvTermsConditionsTxt)
                .setOnLinkClickListener(urlClickListener);
    }

    private void verifyPhoneNumberWithCode(String code) {
       // PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
       // signInWithPhoneAuthCredential(credential);
        getServiceResponseData(code);

    }

    private void getServiceResponseData(String code) {
        InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
        System.out.println("mobile_no "+mobile_no);
        Call<StatusModel> call = api.checkOTP(ApiClient.Apikey, code,mobile_no);
        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                final StatusModel status = response.body();

                if (status.getStatus().equals("1")) {
                    if (activity != null) {
                        activity.startActivity(new Intent(activity, DashboardActivity.class));
                        activity.finish();
                    }
                    // Toast.makeText(VerificationActivity.this, status.getMessage(), Toast.LENGTH_SHORT).show();
                    //  openActivity(data.getResponse().get(0).getEmp_id(),data.getResponse().get(0).getMob_no());

                } else {
                    Toast.makeText(getContext(), status.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
            }
        });
    }

    private void startPhoneNumberVerification(String selectedCountryCode, String phoneNumber,
                                              boolean resendCode, PhoneAuthProvider.ForceResendingToken token) {
        PhoneNumberUtil pnu = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber number;
        try {
            number = pnu.parse(phoneNumber, selectedCountryCode);
            String formattedCarrierNumber = pnu.format(number, PhoneNumberUtil.PhoneNumberFormat.E164);
            if (resendCode) {//Resend OTP
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        formattedCarrierNumber,
                        30,
                        TimeUnit.SECONDS,
                        activity,
                        mCallbacks,
                        token);//resend token
            } else {
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        formattedCarrierNumber,
                        30,
                        TimeUnit.SECONDS,
                        activity,
                        mCallbacks);
            }
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
    }


    private void registerWithEmailAuthCredential(String email, String password, boolean createUser) {
        if (createUser) {//creates user
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(activity, task -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            firebaseUser = task.getResult().getUser();
                            saveInfoAndProceed(firebaseUser, task);
                        } else {
                            Toast.makeText(activity, "Sorry! Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {//signs in user for given email and password
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(activity, task -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            firebaseUser = task.getResult().getUser();
                            saveInfoAndProceed(firebaseUser, task);
                        } else {
                            etLoginPassword.setError("Incorrect password!Try again.");
                        }
                    });
        }
    }

    private void saveInfoAndProceed(FirebaseUser user, Task<AuthResult> task) {
        if (task.getResult() != null &&
                task.getResult().getAdditionalUserInfo() != null) {
            boolean isNewUser = task.getResult().getAdditionalUserInfo().isNewUser();//checks whether new user or not.
            retrieveIDToken(user);
            if (isNewUser) {
                if (registerOTPContainer.getVisibility() == View.VISIBLE) {
                    registerOTPContainer.setVisibility(View.GONE);
                } else if (registerPasswordContainer.getVisibility() == View.VISIBLE) {
                    registerPasswordContainer.setVisibility(View.GONE);
                }
                registerNameContainer.setVisibility(View.VISIBLE);
            } else {
                if (user.getPhoneNumber() != null)
                    Toast.makeText(activity, "Welcome back ".concat(user.getPhoneNumber()), Toast.LENGTH_LONG).show();
                else if (user.getEmail() != null)
                    Toast.makeText(activity, "Welcome back ".concat(user.getEmail()), Toast.LENGTH_LONG).show();

                openDashboardActivity(user);
            }
        }
    }

    private void retrieveIDToken(FirebaseUser firebaseUser) {
        firebaseUser.getIdToken(true).addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null) {
                String idToken = task.getResult().getToken();
                if (idToken != null) preferenceHelper.setUser_token_id(idToken);
                else preferenceHelper.setUser_token_id(""); //Token ID is not available
            } else preferenceHelper.setUser_token_id(""); //Token ID is not available
        });
    }

    private void checkEmailExists(String email) {
        mAuth.fetchSignInMethodsForEmail(email)
                .addOnCompleteListener(task -> {
                    if (task.getResult() != null && task.getResult().getSignInMethods() != null) {
                        boolean isNewUser = task.getResult().getSignInMethods().isEmpty();
                        if (isNewUser) {
                            registerPasswordContainer.setVisibility(View.VISIBLE);
                        } else {
                            loginUserContainer.setVisibility(View.VISIBLE);
                        }
                        registerEmailContainer.setVisibility(View.GONE);
                    }
                })
                .addOnFailureListener(e -> Toast.makeText(activity, "Please enter a valid email id", Toast.LENGTH_SHORT)
                        .show());
    }

    private void openDashboardActivity(FirebaseUser user) {
       // if (user.getPhoneNumber() != null)
        //    preferenceHelper.setUser_phone_number(user.getPhoneNumber());
        //if (user.getEmail() != null)
        //   preferenceHelper.setUser_email(user.getEmail());
        preferenceHelper.setUser_name(etUserName.getText().toString());
       // preferenceHelper.setUser_logged_in(true);

        getView().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity != null) {
                    activity.startActivity(new Intent(activity, DashboardActivity.class));
                    activity.finish();
                }
            }
        }, 2500);

    }

    private void startTimer() {
        tvResendCounterView.setVisibility(View.VISIBLE);
        otpCountdownTimer = new CountDownTimer(30000, 1000) {//30 secs with 1 step sec interval.
            public void onTick(long millisUntilFinished) {
                String text = String.format(Locale.ENGLISH, "%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                tvResendCounterView.setText(text);

//                if (text.equalsIgnoreCase("00 : 26")){
//                    registerOtpView.setText("251357");
//                    openDashboardActivity(firebaseUser);}

            }

            public void onFinish() {
                tvResendCounterView.setText(R.string.resend_otp);
            }
        };
        otpCountdownTimer.start();
    }

    private void sendOtpAPI() {


        InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
        Call<OtpModel> call = api.sendOtp(ApiClient.Apikey, etCarrierNumber.getText().toString());
        mobile_no=etCarrierNumber.getText().toString();
        call.enqueue(new Callback<OtpModel>() {
            @Override
            public void onResponse(Call<OtpModel> call, Response<OtpModel> response) {
                final OtpModel status = response.body();
                if (status.getStatus().equals("1")) {
                    System.out.println("sms Recieved");
                    StartSMSRetriver();
                    Toast.makeText(getContext(), status.getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getContext(), status.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onFailure(Call<OtpModel> call, Throwable t) {
            }
        });

    }

    private void StartSMSRetriver() {
        // Get an instance of SmsRetrieverClient, used to start listening for a matching
// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(activity);

// Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("mobile_no retrieve");
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
    }

    protected void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(@NotNull View view) {
                // Do something with span.getURL() to handle the link click...
                Log.wtf("Onboad", span.getURL());
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    @OnClick(R.id.btn_register_mobile)
    void onRegisterMobileClick() {
        registerUserContainer.setVisibility(View.GONE);
        registerMobileContainer.setVisibility(View.VISIBLE);
        getMobileNumber();
        
        CommonExtensionsKt.showKeyboard(activity, registerUserContainer);
    }

    private void getMobileNumber() {
        getCreadenticalApiClient();
        HintRequest hintRequest = new HintRequest.Builder()
                .setHintPickerConfig(new CredentialPickerConfig.Builder()
                        .setShowCancelButton(true)
                        .build())
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent =
                Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), RESOLVE_HINT, null, 0, 0, 0,new Bundle());
        } catch (IntentSender.SendIntentException e) {
            Log.e("Login", "Could not start hint picker Intent", e);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
                etCarrierNumber.setText(cred.getId().substring(3));
            }
        }
    }

    private void getCreadenticalApiClient() {
       googleApiClient= new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
    }
    @OnClick(R.id.btn_register_email)
    void onRegisterMailClick() {
        registerUserContainer.setVisibility(View.GONE);
        registerEmailContainer.setVisibility(View.VISIBLE);
        CommonExtensionsKt.showKeyboard(activity, registerUserContainer);
    }


    @OnClick(R.id.btn_send_otp)
    void sendOTP() {
        if (countryCodePicker.isValidFullNumber()) {
            tvHeaderOTP.setText(getString(R.string.content_otp).concat("  ").concat(etCarrierNumber.getText().toString()));
            registerOTPContainer.setVisibility(View.VISIBLE);
            registerMobileContainer.setVisibility(View.GONE);
            sendOtpAPI();
            startTimer();
        } else Toast.makeText(activity, "Please enter a valid number.", Toast.LENGTH_SHORT)
                .show();
        CommonExtensionsKt.hideKeyBoard(activity, registerMobileContainer);
    }

    @OnClick(R.id.btn_register_email_next)
    void verifyEmail() {
        if (!etUserEmail.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher
                (etUserEmail.getText().toString()).matches()) {
            String email = etUserEmail.getText().toString().trim();
            tvUserEmail.setText(email);
            tvLoginEmail.setText(email);
            checkEmailExists(email);
        } else Toast.makeText(activity, "Please enter an email id", Toast.LENGTH_SHORT)
                .show();

        CommonExtensionsKt.hideKeyBoard(activity, registerEmailContainer);
    }

    @OnClick(R.id.btn_register_password_next)
    void verifyRegisterPassword() {
        String password = etUserPassword.getText().toString();
        String confirmPassword = etUserConfirmPassword.getText().toString();
        if (!password.isEmpty() && !confirmPassword.isEmpty()) {
            if (password.matches(confirmPassword)) {
                registerWithEmailAuthCredential(tvUserEmail.getText().toString(), confirmPassword, true);
            } else Toast.makeText(activity, "Your password does not match.", Toast.LENGTH_SHORT)
                    .show();
        } else Toast.makeText(activity, "Please enter password", Toast.LENGTH_SHORT)
                .show();

        CommonExtensionsKt.hideKeyBoard(activity, registerPasswordContainer);
    }

    @OnClick(R.id.btn_login)
    void verifyEmailLogin() {
        String userPasswrd = etLoginPassword.getText().toString();
        if (!userPasswrd.isEmpty())
            registerWithEmailAuthCredential(tvLoginEmail.getText().toString(), etLoginPassword.getText().toString(), false);
        else Toast.makeText(activity, "Please enter your password", Toast.LENGTH_SHORT)
                .show();
        CommonExtensionsKt.hideKeyBoard(activity, registerOTPContainer);
    }

    @OnClick(R.id.btn_verify_otp)
    void verifyOTP() {
        if (registerOtpView.getText() != null && !registerOtpView.getText().toString().isEmpty())
            //openDashboardActivity(firebaseUser);
            verifyPhoneNumberWithCode(registerOtpView.getText().toString());
        else
            Toast.makeText(activity, "Please enter the OTP sent to the mobile number", Toast.LENGTH_SHORT)
                    .show();

        CommonExtensionsKt.hideKeyBoard(activity, registerOTPContainer);
    }

    @OnClick(R.id.tv_resend_counter)
    void resendOTP() {
        startPhoneNumberVerification(countryCodePicker.getSelectedCountryNameCode(),
                countryCodePicker.getFullNumberWithPlus(), true, mResendToken);

        CommonExtensionsKt.hideKeyBoard(activity, registerOTPContainer);
    }

    @OnClick(R.id.btn_register_continue)
    void registerContinue() {
        String name = etUserName.getText().toString();
        if (!name.isEmpty()) {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(name).build();
            FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates);
            openDashboardActivity(firebaseUser);
        } else Toast.makeText(activity, "Please enter a valid name.", Toast.LENGTH_SHORT)
                .show();
    }

    @OnClick({R.id.btn_back, R.id.iv_dismiss_dialog, R.id.btn_otp_back, R.id.btn_register_email_back,
            R.id.btn_register_password_back, R.id.btn_login_back, R.id.btn_register_back})
    void onBackClick() {
        if (registerMobileContainer.getVisibility() == View.VISIBLE) {
            registerUserContainer.setVisibility(View.VISIBLE);
            registerMobileContainer.setVisibility(View.GONE);
        } else if (registerOTPContainer.getVisibility() == View.VISIBLE) {
            registerOTPContainer.setVisibility(View.GONE);
            registerMobileContainer.setVisibility(View.VISIBLE);
        } else if (registerNameContainer.getVisibility() == View.VISIBLE) {
            registerNameContainer.setVisibility(View.GONE);
            registerOTPContainer.setVisibility(View.VISIBLE);
        } else if (registerEmailContainer.getVisibility() == View.VISIBLE) {
            registerUserContainer.setVisibility(View.VISIBLE);
            registerEmailContainer.setVisibility(View.GONE);
        } else if (registerPasswordContainer.getVisibility() == View.VISIBLE) {
            registerEmailContainer.setVisibility(View.VISIBLE);
            registerPasswordContainer.setVisibility(View.GONE);
        } else if (loginUserContainer.getVisibility() == View.VISIBLE) {
            registerEmailContainer.setVisibility(View.VISIBLE);
            loginUserContainer.setVisibility(View.GONE);
        } else {
            dismiss();
        }
        CommonExtensionsKt.hideKeyBoard(activity, registerUserContainer);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Do Nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Do Nothing
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etCarrierNumber.getText().hashCode() == editable.hashCode()) {
            if (editable.toString().length() >= 8) btnSendOtp.setEnabled(true);
            else btnSendOtp.setEnabled(false);
        } else if (registerOtpView.getText() != null && registerOtpView.getText().hashCode() == editable.hashCode()) {
            if (editable.toString().length() == 6) btnVerifyOtp.setEnabled(true);
            else btnVerifyOtp.setEnabled(false);
        } else if (etUserName.getText().hashCode() == editable.hashCode()) {
            if (editable.toString().length() >= 4) btnRegisterContinue.setEnabled(true);
            else btnRegisterContinue.setEnabled(false);
        } else if (etUserEmail.getText().hashCode() == editable.hashCode()) {
            if (editable.toString().length() >= 4) btnRegisterEmailNxt.setEnabled(true);
            else btnRegisterEmailNxt.setEnabled(false);
        } else if (etUserConfirmPassword.getText().hashCode() == editable.hashCode()) {
            if (editable.toString().length() >= 3 &&
                    editable.toString().matches(etUserPassword.getText().toString())) {
                btnRegisterPasswordNxt.setEnabled(true);
                tvPasswordMatchTxt.setVisibility(View.VISIBLE);
            } else {
                tvPasswordMatchTxt.setVisibility(View.INVISIBLE);
                btnRegisterPasswordNxt.setEnabled(false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (otpCountdownTimer != null)
            otpCountdownTimer.cancel();
    }


    @Override
    public void onOtpReceived(String otp) {
        System.out.println("Recieved..... "+otp);
        registerOtpView.setText(otp);
    }

    @Override
    public void onOtpTimeout() {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
