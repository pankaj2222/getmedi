package storesOpenIn.com.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import storesOpenIn.com.R;
import storesOpenIn.com.dashboard.DashboardActivity;
import storesOpenIn.com.language.LanguageActivity;
import storesOpenIn.com.utils.PreferenceHelper;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

public class LauncherActivity extends AppCompatActivity {

    private PreferenceHelper preferenceHelper;
    private Handler mHandler = new Handler();
    private Runnable startActivityTask = new Runnable() {
        public void run() {
            if (preferenceHelper.getUser_logged_in()) {
                startActivity(new Intent(LauncherActivity.this, DashboardActivity.class).setFlags(
                        FLAG_ACTIVITY_CLEAR_TASK));
            } else
                startActivity(new Intent(LauncherActivity.this, LanguageActivity.class).setFlags(FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_launcher);
        preferenceHelper = new PreferenceHelper(LauncherActivity.this);
        //retrieveIDToken(FirebaseAuth.getInstance().getCurrentUser());
        mHandler.postDelayed(startActivityTask, 2000);
        // ATTENTION: This was auto-generated to handle app links.
    }

}
