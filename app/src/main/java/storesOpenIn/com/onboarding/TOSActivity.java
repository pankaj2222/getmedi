package storesOpenIn.com.onboarding;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import storesOpenIn.com.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TOSActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_service);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.iv_back)
    void onBackClick(){
        onBackPressed();
    }
}
