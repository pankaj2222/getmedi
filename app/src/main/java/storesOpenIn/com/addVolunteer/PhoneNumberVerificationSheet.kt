package storesOpenIn.com.addVolunteer

import android.app.Dialog
import android.os.Bundle
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.layout_phone_verification.*
import storesOpenIn.com.R
import storesOpenIn.com.utils.setVisible
import storesOpenIn.com.widgets.otpview.OtpView

/**
 * Created by BalaKrishnan
 */
class PhoneNumberVerificationSheet : BottomSheetDialogFragment() {
    var phoneNumber: String? = null
    var verificationToken: String? = null
    var registerOtpView: OtpView? = null
    var phoneNumberVerificationUtil: PhoneNumberVerificationUtil? = null
    var dialog: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        retainInstance = true
        if (dialog == null)
            dialog = inflater.inflate(R.layout.layout_phone_verification, container, false)
        return dialog
    }


    var listener: PhoneNumberVerificationUtil.AuthenticationResult? = null

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvStatus.text = "Sending otp to ${phoneNumber?.replace("+91", "")}"
        registerOtpView = view.findViewById(R.id.register_otp_view)
        ivClose.setOnClickListener {
            dismiss()
        }
        phoneNumberVerificationUtil = PhoneNumberVerificationUtil(object :
            PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                val code = credential.smsCode
                if (code != null) {
                    registerOtpView?.setText(code)
                    TransitionManager.beginDelayedTransition(bottomSheetContainer)
                    btnCodeVerify.alpha =1.0f
                    phoneNumberVerificationUtil?.verifyPhoneNumberWithCode(
                        activity!!,
                        verificationToken,
                        code
                    )
                }
            }

            override fun onVerificationFailed(exception: FirebaseException) {
                listener?.failed(exception = exception.message)
            }

            override fun onCodeSent(
                verificationID: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                Log.d(TAG, " onCodeSent")
                verificationToken = verificationID
                TransitionManager.beginDelayedTransition(bottomSheetContainer)
                btnCodeVerify.alpha =1.0f
                tvStatus.text = "Enter otp sent to ${phoneNumber?.replace("+91", "")}"
            }

        }, listener = listener)

        phoneNumberVerificationUtil?.startPhoneNumberVerification(activity, "IN", phoneNumber)

        btnCodeVerify.setOnClickListener {
            try {
                phoneNumberVerificationUtil?.verifyPhoneNumberWithCode(
                    activity!!,
                    verificationToken,
                    register_otp_view.text.toString()
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


    }

    companion object {
        val TAG = javaClass.name
    }
}