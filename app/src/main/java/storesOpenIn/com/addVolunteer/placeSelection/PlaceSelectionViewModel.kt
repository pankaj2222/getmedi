package storesOpenIn.com.addVolunteer.placeSelection

import android.app.Application
import android.widget.SearchView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.addVolunteer.placeSelection.data.Data
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLongWrapper
import storesOpenIn.com.addVolunteer.placeSelection.data.PlacesAPI
import storesOpenIn.com.utils.*

/**
 * Created by BalaKrishnan
 */
class PlaceSelectionViewModel(application: Application) : AndroidViewModel(application) {
    private var launch: Job? = null
    val data = MutableLiveData<List<Data>>()

    private val _navigationWithParam = MutableLiveData<Event<Navigation>>()
    val navigationWithParamEvent: LiveData<Event<Navigation>> = _navigationWithParam
    private val TAG = javaClass.name

    fun onAddClick(data: Data) {
        if (getApplication<BaseApplication>().applicationContext.isOnline()) {
            viewModelScope.launch {
                val latLongData = RetrofitUtil.provideRetrofit().create(PlacesAPI::class.java)
                    .getLatLong(
                        "https://covid-open-in.netlify.com/.netlify/functions/getcoordinates",
                        data.place_id
                    )

                _navigationWithParam.value =
                    Event(
                        Navigation(
                            Action.PLACE_SELECTION_ADDED,
                            LatLongWrapper(latLongData, data.structured_formatting.main_text)
                        )
                    )

            }
        }
    }

    fun setQueryListener(): androidx.appcompat.widget.SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                _navigationWithParam.value = Event(Navigation(Action.LOADING, null))
                launch?.cancel()
                if (newText.isNullOrEmpty()) {
                    data.postValue(listOf())
                    _navigationWithParam.value = Event(Navigation(Action.LOADING_DONE, null))
                    return false
                }

                launch = viewModelScope.launch {
                    val contacts = RetrofitUtil.provideRetrofit().create(PlacesAPI::class.java)
                        .search(
                            "https://covid-open-in.netlify.com/.netlify/functions/getpredictions",
                            newText
                        )
                    data.postValue(contacts.data)
                    _navigationWithParam.value = Event(Navigation(Action.LOADING_DONE, null))
                }
                return false
            }
        }

    }
}