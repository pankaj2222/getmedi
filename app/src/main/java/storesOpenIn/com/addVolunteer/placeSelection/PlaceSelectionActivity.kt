package storesOpenIn.com.addVolunteer.placeSelection

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.MaterialToolbar
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_place_selection.*
import storesOpenIn.com.R
import storesOpenIn.com.utils.Action
import storesOpenIn.com.utils.EventObserver
import storesOpenIn.com.utils.KEY_LOCATION
import storesOpenIn.com.utils.executionOnInternet


class PlaceSelectionActivity : AppCompatActivity() {
    private val TAG = javaClass.name
    private val placeSelectionViewModel: PlaceSelectionViewModel by lazy {
        ViewModelProviders.of(this).get(PlaceSelectionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_selection)

        // toolbar
        val toolbar: MaterialToolbar = findViewById<View>(R.id.tbPlaceSelection) as MaterialToolbar
        setSupportActionBar(toolbar)
        // add back arrow to toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        svPlaces.requestFocus()
        val title = intent.getStringExtra(TITLE)
        title?.let {
            toolbar.title = it
        }
        executionOnInternet {
            val adapter = PlaceAdapter(viewModel = placeSelectionViewModel)

            rvPlaces.layoutManager = LinearLayoutManager(this)

            rvPlaces.adapter = adapter

            svPlaces.setOnQueryTextListener(placeSelectionViewModel.setQueryListener())

            placeSelectionViewModel.data.observe(this, Observer {
                adapter
                    .updateContacts(it)
            })
            placeSelectionViewModel.navigationWithParamEvent.observe(this, EventObserver {
                it.let {
                    try {
                        when (it.action) {
                            Action.PLACE_SELECTION_ADDED -> {
                                progressBar.hide()
                                val intent = Intent()
                                intent.putExtra(
                                    KEY_LOCATION,
                                    GsonBuilder().create().toJson(it.param)
                                )
                                setResult(Activity.RESULT_OK, intent)
                                onBackPressed()
                            }
                            Action.LOADING -> {
                                progressBar.show()
                            }
                            Action.LOADING_DONE -> {
                                progressBar.hide()
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        @kotlin.jvm.JvmField
        val TITLE = "TITLE"
    }
}
