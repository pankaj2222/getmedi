package storesOpenIn.com.addVolunteer.placeSelection.data

import androidx.annotation.Keep

/**
 * Created by BalaKrishnan
 */
@Keep
data class LatLong(var lat: Double = 0.0, var lng: Double = 0.0) {
    fun isEmpty(): Boolean {
        return (lat == 0.0 || lng == 0.0)
    }
}