package storesOpenIn.com.addVolunteer.placeSelection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import storesOpenIn.com.BR
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.placeSelection.data.Data
import storesOpenIn.com.utils.AutoUpdatableAdapter
import kotlin.properties.Delegates

/**
 * Created by BalaKrishnan
 */
class PlaceAdapter(val viewModel: PlaceSelectionViewModel) :
    RecyclerView.Adapter<PlaceAdapter.ViewHolder>(),
    AutoUpdatableAdapter {

    var contacts: List<Data> by Delegates.observable(emptyList()) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(
                inflater,
                R.layout.item_place,
                parent,
                false
            )

        return ViewHolder(binding)
    }

    override fun getItemCount() = contacts.size

    /**
     *updates the list and list automatically deploys diffutil
     */
    fun updateContacts(contacts: List<Data>) {
        this.contacts = contacts
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(contacts[position], viewmodel = viewModel)
    }

    inner class ViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(contact: Data, viewmodel: PlaceSelectionViewModel) {
            this.binding.setVariable(BR.responseResult, contact)
            this.binding.setVariable(BR.viewmodel, viewmodel)
            this.binding.executePendingBindings()
        }
    }

}