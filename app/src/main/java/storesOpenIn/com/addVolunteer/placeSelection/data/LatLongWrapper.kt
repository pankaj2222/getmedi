package storesOpenIn.com.addVolunteer.placeSelection.data

import androidx.annotation.Keep

/**
 * Created by BalaKrishnan
 */
@Keep
data class LatLongWrapper<out T>(val data: LatLong, val value: T)