package storesOpenIn.com.addVolunteer.placeSelection.data

import androidx.annotation.Keep

@Keep
data class PlacesResponse(
    val data: List<Data>
)
@Keep
data class Terms(
    val offset: Int,
    val value: String
)
@Keep
data class Structured_formatting(

    val main_text: String,
    val main_text_matched_substrings: List<Main_text_matched_substrings>,
    val secondary_text: String
)
@Keep
data class Main_text_matched_substrings(

    val length: Int,
    val offset: Int
)
@Keep
data class Matched_substrings(
    val length: Int,
    val offset: Int
)

@Keep
data class Data(
    val description: String,
    val id: String,
    val matched_substrings: List<Matched_substrings>,
    val place_id: String,
    val reference: String,
    val structured_formatting: Structured_formatting,
    val terms: List<Terms>,
    val types: List<String>
)