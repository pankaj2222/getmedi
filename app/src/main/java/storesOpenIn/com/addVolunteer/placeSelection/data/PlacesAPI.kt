package storesOpenIn.com.addVolunteer.placeSelection.data

import storesOpenIn.com.addVolunteer.placeSelection.data.PlacesResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by BalaKrishnan
 */
interface PlacesAPI {
    @GET
    suspend fun search(@Url url: String, @Query("search") searchString: String?): PlacesResponse
    @GET
    suspend fun getLatLong(@Url url: String, @Query("placeId") placeID: String?): LatLong
}