package storesOpenIn.com.addVolunteer

import android.app.Activity
import android.util.Log
import storesOpenIn.com.utils.showToast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import java.util.concurrent.TimeUnit

/**
 * Created by BalaKrishnan
 */
class PhoneNumberVerificationUtil(
    mCallbacks: OnVerificationStateChangedCallbacks,
    private val listener: AuthenticationResult?
) {
    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val currentUser: FirebaseUser?
    private val mCallbacks: OnVerificationStateChangedCallbacks
    fun startPhoneNumberVerification(
        activity: Activity?,
        selectedCountryCode: String?,
        phoneNumber: String?
    ) {
        val pnu = PhoneNumberUtil.getInstance()
        val number: PhoneNumber
        try {
            number = pnu.parse(phoneNumber, selectedCountryCode)
            val formattedCarrierNumber =
                pnu.format(number, PhoneNumberUtil.PhoneNumberFormat.E164)
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                formattedCarrierNumber,
                30,
                TimeUnit.SECONDS,
                activity!!,
                mCallbacks
            )
        } catch (e: NumberParseException) {
            listener?.failed("Please check the number")
            e.printStackTrace()
        }
    }

    fun verifyPhoneNumberWithCode(
        activity: Activity,
        mVerificationId: String?,
        code: String?
    ) {
        val credential = if (mVerificationId == null)
            PhoneAuthProvider.getCredential("", code!!)
        else {
            PhoneAuthProvider.getCredential(mVerificationId, code!!)
        }

        signInWithPhoneAuthCredential(activity, credential)

    }

    private fun signInWithPhoneAuthCredential(
        activity: Activity,
        credential: PhoneAuthCredential
    ) {
        if (mAuth.currentUser == null) {
            activity.showToast("Some thing went wrong")
        } else
            mAuth.currentUser!!.linkWithCredential(credential)
                .addOnCompleteListener(activity, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful) {
                            val result = task.result
                            if (result != null) {
                                Log.d(TAG, "signInWithPhoneAuthCredential: ")
                                listener?.success()
                            } else {
                                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                                    listener?.failed("Invalid code.Please verify the OTP code")
                                } else {
                                    listener?.failed("Something went wrong")
                                }
                            }
                        } else {
                            listener?.failed(task.exception?.message)
                        }
                    }

                })
                .addOnFailureListener {
                    Log.e(TAG, " verification failed ", it)
                }

    }

    companion object {
        private const val TAG = "PhoneNumberVerification"
    }

    init {
        currentUser = mAuth.currentUser
        this.mCallbacks = mCallbacks
    }

    interface AuthenticationResult {
        fun success()
        fun failed(exception: String?)
    }
}