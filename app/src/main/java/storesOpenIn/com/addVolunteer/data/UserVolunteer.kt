package storesOpenIn.com.addVolunteer.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class UserVolunteer(
    @SerializedName("data")
    var data: UserData
)

@Keep
class UserData {
    var users: List<User>? = null
}

@Keep
data class User(
    var is_volunteer: Boolean,
    val auth_id: String = "",
    val name: String?,
    val phone: String?,
    val location: Int?
)
