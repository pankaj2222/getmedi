package storesOpenIn.com.addVolunteer.data

import androidx.annotation.Keep

@Keep
data class UpsertLocationResponse(
    val data: Data
)
@Keep
data class Returning(
    var id: Long
)
@Keep
class InsertLocation(
    var returning: List<Returning>
)
@Keep
class Data(
    var insert_location: InsertLocation
)