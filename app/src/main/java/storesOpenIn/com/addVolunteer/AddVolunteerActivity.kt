package storesOpenIn.com.addVolunteer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import com.google.android.material.appbar.MaterialToolbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_add_volunteer.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.data.User
import storesOpenIn.com.addVolunteer.placeSelection.PlaceSelectionActivity
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLongWrapper
import storesOpenIn.com.utils.*
import storesOpenIn.com.utils.PreferenceHelper as Preference


const val LOCATION_REQ_CODE = 101

class AddVolunteerActivity : AppCompatActivity() {
    private val TAG = javaClass.name

    val pref by lazy {
        Preference(this)
    }


    private lateinit var addVolunteerViewModel: AddVolunteerViewModel

    var fromProfile = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_volunteer)

        fromProfile = intent.getBooleanExtra(KEY_FROM_PROFILE, false)

        btnDeleteVolunteer.setVisible(fromProfile)
        if (fromProfile) {
            mBtnJoin.text = resources.getText(R.string.done)
            tbVolunteer.title = getString(R.string.edit_volunteer)
        }

        // toolbar
        val toolbar: MaterialToolbar = findViewById<View>(R.id.tbVolunteer) as MaterialToolbar
        setSupportActionBar(toolbar)
        // add back arrow to toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        executionOnInternet {
            addVolunteerViewModel =
                ViewModelFactory(AddVolunteerVMData(pref, intent)).create(
                    AddVolunteerViewModel::class.java
                )

            addVolunteerViewModel.userVolunteer.observe(this, Observer { it ->
                if (it == null) return@Observer
                if (it.is_volunteer) {                              //
                    if (!fromProfile) {                             //
                        showToast(getString(R.string.volunteer_already))    //
                        finish()                                    //
                    } else {
                        pageSetup(it)
                        addVolunteerViewModel.getLocationName(it.location!!)
                            .observe(this, EventObserver { list ->
                                list?.let {
                                    val latLong = LatLong(list[0], list[1])
                                    val decodeGeo = LocationDecode.decodeGeo(
                                        this,
                                        latLong
                                    )

                                    btnLocality.text = decodeGeo

                                    addVolunteerViewModel.locationValue =
                                        LatLongWrapper(latLong, decodeGeo)

                                    progressBar.hide()
                                }
                            })
                    }
                } else {
                    if (!fromProfile)
                        pageSetup(it)
                    else {
                        showToast(getString(R.string.not_volunteer_yet))
                        finish()
                    }
                }
            })
        }

    }

    fun pageSetup(it: User) {

        btnLocality.setOnClickListener {
            startActivityForResult(
                Intent(this, PlaceSelectionActivity::class.java),
                LOCATION_REQ_CODE
            )
        }

        it.name?.let { name ->
            tietName.setText(name)
        }
        it.phone?.let { phone ->

            if (phone.isEmpty() || phone == "0000") {
                btnVerify.setVisible(true)
                tietPhone.setText("")
                tietPhone.isEnabled = true
            } else {
                btnVerify.setVisible(false)
                tilPhone.alpha = .5f
                tietPhone.alpha = .5f
                tietPhone.setText(phone)
                tietPhone.isEnabled = false
            }

        }

        val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val phone = tietPhone.text.toString()
                val name = tietName.text.toString()
                mBtnJoin.isEnabled =
                    (phone.isNotEmpty() && name.isNotEmpty() && addVolunteerViewModel.locationValue != null)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        }

        tietName.addTextChangedListener {
            textWatcher
        }
        tietPhone.addTextChangedListener {
            textWatcher
        }

        mBtnJoin.setOnClickListener {
            executionRequireInternet {
                val profileUpdates = UserProfileChangeRequest.Builder()
                    .setDisplayName(tietName.text.toString()).build()
                FirebaseAuth.getInstance().currentUser!!.updateProfile(profileUpdates)
                addVolunteerViewModel.makeAddVolunteerCall(name = "" + tietName.text)
            }
        }

        tietPhone.addTextChangedListener(afterTextChanged = {
            btnVerify.setVisible(addVolunteerViewModel.instance.currentUser?.phoneNumber != it.toString())
        })

        addVolunteerViewModel.navigationWithParamEvent.observe(this, EventObserver {
            it.let {
                try {
                    when (it.action) {
                        Action.LOADING -> {
                            progressBar.show()
                        }
                        Action.LOADING_DONE -> {
                            progressBar.hide()
                        }
                        Action.ERROR -> {
                            showToast(it.param as String)
                        }
                        Action.VOLUNTEER_REMOVED -> {
                            (application as BaseApplication).isVolunteer.postValue(false)
                            showToast(getString(R.string.removed_from_volunteer))
                            finish()
                        }
                        Action.VOLUNTEER_ADDED -> {
                            (application as BaseApplication).isVolunteer.postValue(true)
                            clContainer.showVolunteerPopup(
                                PopupData(
                                    drawable = R.drawable.ic_volunteer_add_succes,
                                    title = getString(R.string.volunteer_added_success),
                                    desc = getString(R.string.volunteer_added_success_desc),
                                    shareBtnString = getString(R.string.share_the_news),
                                    clickListener = object : PopupData.ClickListener {
                                        override fun onClick(v: View) {
                                            if (v.id == R.id.btnSuccessShare) {
                                                shareApp(getString(R.string.volunteerShare))
                                            }
                                        }
                                    }
                                )
                            )
                        }
                        else -> {

                        }
                    }
                } catch (e: Exception) {
                }
            }
        })
        btnVerify.setOnClickListener {
            executionRequireInternet {
                val replace = tietPhone.text.toString().replace("+91", "");
                if (replace.length == 10)
                    verifyMobileNumber()
                else {
                    showToast(getString(R.string.check_number))
                }
            }
        }
        btnDeleteVolunteer.setOnClickListener {
            executionRequireInternet {
                addVolunteerViewModel.makeAddVolunteerCall("", false)
            }
        }

    }

    private fun verifyMobileNumber() {
        val bottomDialogFragment = PhoneNumberVerificationSheet()
        bottomDialogFragment.phoneNumber = "+91" + tietPhone.text
        bottomDialogFragment.listener =
            object : PhoneNumberVerificationUtil.AuthenticationResult {
                override fun success() {
                    bottomDialogFragment.dismiss()
                    btnVerify.text = getString(R.string.verified)
                    btnVerify.isClickable = false
                }

                override fun failed(exception: String?) {
                    if (exception.isNullOrEmpty())
                        showToast(getString(R.string.something_went_wrong))
                    else
                        showToast(exception)
                }

            }
        bottomDialogFragment.show(
            supportFragmentManager,
            PhoneNumberVerificationSheet.TAG
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LOCATION_REQ_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val gson = GsonBuilder().create()
                addVolunteerViewModel.locationValue =
                    gson.fromJson(
                        data?.getStringExtra(KEY_LOCATION),
                        LatLongWrapper::class.java
                    ) as LatLongWrapper<String>

                btnLocality.text = addVolunteerViewModel.locationValue?.value
                btnLocality.setTextColor(ContextCompat.getColor(this, R.color.textColorBold))
                mBtnJoin.isEnabled =
                    (tietPhone.text.toString().isNotEmpty() && tietName.text.toString()
                        .isNotEmpty() && addVolunteerViewModel.locationValue != null)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
