package storesOpenIn.com.addVolunteer

import android.content.Intent
import android.util.Log
import androidx.annotation.Keep
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import storesOpenIn.com.addVolunteer.data.UpsertLocationResponse
import storesOpenIn.com.addVolunteer.data.User
import storesOpenIn.com.addVolunteer.data.UserVolunteer
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLongWrapper
import storesOpenIn.com.utils.*
import storesOpenIn.com.utils.APIQuery.getUserDetails
import storesOpenIn.com.utils.APIQuery.updateUserWOPhoneNumber
import storesOpenIn.com.utils.APIQuery.updateUserWithPhoneNumber
import storesOpenIn.com.utils.APIQuery.upsertLocationQuery
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.launch
import okhttp3.RequestBody


/**
 * Created by BalaKrishnan
 */

data class AddVolunteerVMData(val preferenceHelper: PreferenceHelper, val intent: Intent)

class AddVolunteerViewModel(addVolunteerVMData: AddVolunteerVMData) : ViewModel() {
    val instance: FirebaseAuth = FirebaseAuth.getInstance()
    var locationValue: LatLongWrapper<String>? = null
    private val TAG = javaClass.name
    private val _navigationWithParam = MutableLiveData<Event<Navigation>>()
    val navigationWithParamEvent: LiveData<Event<Navigation>> = _navigationWithParam
    val userVolunteer = MutableLiveData<User>(null)

    val gson = GsonBuilder().create()

    val token =
        "Bearer " + addVolunteerVMData.preferenceHelper.user_token_id

    var userWithMN = false

    var fromProfile = false

    init {
        userWithMN = FirebaseAuth.getInstance().currentUser?.phoneNumber.isNullOrEmpty().not()
        fromProfile = addVolunteerVMData.intent.getBooleanExtra(KEY_FROM_PROFILE, false)
        isUserVolunteer()
    }

    fun makeAddVolunteerCall(name: String, makeVolunteer: Boolean = true) {
        if (makeVolunteer) {
            if (locationValue == null) {
                _navigationWithParam.value =
                    Event(Navigation(Action.ERROR, "Please select a location/area"))
                return
            }
            if (name.isNullOrEmpty()) {
                _navigationWithParam.value =
                    Event(Navigation(Action.ERROR, "Name cannot be empty"))
                return
            }
            val phoneNumber = instance.currentUser?.phoneNumber
            if (phoneNumber.isNullOrEmpty()) {
                _navigationWithParam.value =
                    Event(Navigation(Action.ERROR, "Please verify the Mobile number"))
                return
            }

            viewModelScope.launch {
                val postValue = RetrofitUtil.getGraphQLAPI()
                    .postValue(
                        value = token,
                        requestBody = upsertLocationQuery(
                            locationValue?.data?.lat!!,
                            locationValue?.data?.lng!!
                        )
                    )

                val locationIdModel =
                    gson.fromJson(postValue.string(), UpsertLocationResponse::class.java)

                if (locationIdModel == null) {
                    _navigationWithParam.value =
                        Event(Navigation(Action.ERROR, "Something went wrong"))
                    return@launch
                }

                RetrofitUtil.getGraphQLAPI().postValue(
                    value = token,
                    requestBody =
                    if (userWithMN) {
                        updateUserWithPhoneNumber(
                            name,
                            locationIdModel.data.insert_location.returning[0].id.toInt(),
                            true
                        )
                    } else {
                        updateUserWOPhoneNumber(
                            name,
                            phoneNumber,
                            locationIdModel.data.insert_location.returning[0].id.toInt(),
                            true
                        )
                    }

                )

                _navigationWithParam.value =
                    Event(Navigation(Action.VOLUNTEER_ADDED, null))

            }
        }else{
           viewModelScope.launch {

               RetrofitUtil.getGraphQLAPI().postValue(
                   value = token,
                   requestBody =
                       APIQuery.updateVolunteer(false)
               )

               _navigationWithParam.value =
                   Event(Navigation(Action.VOLUNTEER_REMOVED, null))

           }
        }
    }


    fun isUserVolunteer() {
        viewModelScope.launch {
            val string = RetrofitUtil.getGraphQLAPI().postValue(
                value = token,
                requestBody = getUserDetails()
            ).string()
            val fromJson = gson.fromJson(string, UserVolunteer::class.java)
            Log.d(TAG, " value ${gson.toJson(fromJson)}")
            fromJson?.data?.users?.let {
                if (it.isNotEmpty())
                    userVolunteer.postValue(it[0])
            }
        }
    }

    fun getLocationName(location: Int): LiveData<Event<List<Double>?>> {
        val liveData = MutableLiveData<Event<List<Double>?>>()
        viewModelScope.launch {
            _navigationWithParam.postValue(Event(Navigation(Action.LOADING, null)))
            val body = RetrofitUtil.getGraphQLAPI()
                .postValue(value = token, requestBody = APIQuery.getLocationDetails(location))
            val locationDetails = body.string().fromJson(LocationDetails::class.java)
            val coordinates = locationDetails?.data?.location?.get(0)?.location?.coordinates
            liveData.postValue(Event(coordinates))
        }
        return liveData
    }
}

@Keep
data class LocationDetails(
    @SerializedName("data")
    val `data`: Data = Data()
)

@Keep
data class Data(
    @SerializedName("location")
    val location: List<Location> = listOf()
)

@Keep
data class Location(
    @SerializedName("location")
    val location: LocationX = LocationX()
)

@Keep
data class LocationX(
    @SerializedName("coordinates")
    val coordinates: List<Double> = listOf(),
    @SerializedName("crs")
    val crs: Crs = Crs(),
    @SerializedName("type")
    val type: String = "" // Point
)

@Keep
data class Crs(
    @SerializedName("properties")
    val properties: Properties = Properties(),
    @SerializedName("type")
    val type: String = "" // name
)

@Keep
data class Properties(
    @SerializedName("name")
    val name: String = "" // urn:ogc:def:crs:EPSG::4326
)