package storesOpenIn.com
import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName


/**
 * Created by BalaKrishnan
 */
@Keep
data class ErrorResponse(
    @SerializedName("errors")
    val errors: List<Error> = listOf()
)

@Keep
data class Error(
    @SerializedName("extensions")
    val extensions: Extensions = Extensions(),
    @SerializedName("message")
    val message: String = "" // Could not verify JWT: JWTExpired
)

@Keep
data class Extensions(
    @SerializedName("code")
    val code: String = "", // invalid-jwt
    @SerializedName("path")
    val path: String = "" // $
)