package storesOpenIn.com.volunteerListing

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import storesOpenIn.com.storeListing.LocationByLocation

@Keep
data class VolunteerListData(
    val data: Data = Data()
)

@Keep
data class Data(
    val users:
    List<User> = listOf()
)

@Keep
data class User(
    val id: Long,
    val location: Long,
    val name: String,
    val phone: String,
    @SerializedName("locationByLocation")
    val locationByLocation: LocationByLocation = LocationByLocation()
)