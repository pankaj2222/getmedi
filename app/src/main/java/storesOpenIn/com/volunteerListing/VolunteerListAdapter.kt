package storesOpenIn.com.volunteerListing

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import storesOpenIn.com.BR
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.utils.AutoUpdatableAdapter
import storesOpenIn.com.utils.LocationDecode
import kotlin.properties.Delegates


/**
 * Created by BalaKrishnan
 */
class VolunteerListAdapter(val viewModel: VolunteerListingViewModel) :
    RecyclerView.Adapter<VolunteerListAdapter.ViewHolder>(),
    AutoUpdatableAdapter {
    private val TAG = javaClass.name
    var contacts: List<User> by Delegates.observable(emptyList()) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(
                inflater,
                R.layout.item_volunteer,
                parent,
                false
            )

        return ViewHolder(binding)
    }

    override fun getItemCount() = contacts.size

    /**
     *updates the list and list automatically deploys diffutil
     */
    fun updateContacts(contacts: List<User>) {
        this.contacts = contacts
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(contacts[position], viewmodel = viewModel)
        holder.itemView.findViewById<ImageView>(R.id.ivCall).setOnClickListener {
            viewModel.makePhoneCall(contacts[position].phone)
        }
    }

    inner class ViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(contact: User, viewmodel: VolunteerListingViewModel) {
            val coordinates = contact.locationByLocation.location.coordinates
            val context = binding.root.context

            val address = LocationDecode.getAddress(
                context,
                LatLong(
                    coordinates[0],
                    coordinates[1]
                )
            )

            val split = address.getAddressLine(0).split(",")

            val size = split.size

            var locality = if (size > 0) {
                if (size < 2) {
                    split[0]
                } else {
                    split[0] + "," + split[1]
                }
            } else {
                address.locality
            }

            if (locality.isNullOrEmpty()) {
                locality = address.featureName
            }

            this.binding.setVariable(BR.responseResult, contact)
            this.binding.setVariable(BR.volunteerViewmodel, viewmodel)
            this.binding.setVariable(BR.locality, locality)
            this.binding.executePendingBindings()
        }
    }

}