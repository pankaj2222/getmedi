package storesOpenIn.com.volunteerListing

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volunteer_listing.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.AddVolunteerActivity
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.utils.ConnectionStateMonitor
import storesOpenIn.com.utils.Status
import storesOpenIn.com.utils.emptyState.EmptyStateContent
import storesOpenIn.com.utils.emptyState.EmptyStateHelper
import storesOpenIn.com.utils.location.LocationHelper
import storesOpenIn.com.utils.setVisible

class VolunteerListingActivity : AppCompatActivity() {
    lateinit var helper: LocationHelper
    private val addVolunteerViewModel: VolunteerListingViewModel by lazy {
        ViewModelProviders.of(this).get(VolunteerListingViewModel::class.java)
    }
    val emptyStateHelper by lazy { EmptyStateHelper(this@VolunteerListingActivity) }

    private val TAG = javaClass.name
    val connectionStateMonitor: ConnectionStateMonitor by lazy {
        ConnectionStateMonitor(this)
    }
    var phone: String = ""
    lateinit var adapter: VolunteerListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volunteer_listing)


        connectionStateMonitor.observe(this, Observer {
            it?.let {
                if (!it) {
                    val value = addVolunteerViewModel.responseData.value
                    if (value == null || value.data == null) {
                        rvNearMeVolunteers.setVisible(false)
                        tvCount.text = "No internet connection please check"
                        tvCount.textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }
                }
            }

        })
        adapter = VolunteerListAdapter(addVolunteerViewModel)
        helper = LocationHelper(this)
        helper.getLocationPermission()

        val location = (application as BaseApplication).location
        if (location != null)
            addVolunteerViewModel.getNearMeVolunteers(LatLong(location.lat, location.lng))

        helper.observeLocation().observe(this, Observer {
            it?.let {
            }
        })

        helper.observeLocationPermissionStatus().observe(this, Observer {
            helper.getDeviceLocation()
        })
        rvNearMeVolunteers.adapter = adapter
        addVolunteerViewModel.responseData.observe(this, Observer { it ->
            when (it.status) {
                Status.SUCCESS -> {
                    val toMutableList = it?.data?.data?.users?.toMutableList()
                    var isVolunteer = false
                    (application as BaseApplication).isVolunteer.value?.let { isValue ->
                        isVolunteer = isValue
                    }
                    toMutableList?.let { it ->
                        if (it.isEmpty()) {
                            emptyStateHelper.showEmptyState(
                                flState,
                                EmptyStateContent.getData(
                                    this@VolunteerListingActivity,
                                    if (isVolunteer) EmptyStateContent.EMPTY_STATE_VOLUNTEER_LISTING_NOCTA else EmptyStateContent.EMPTY_STATE_VOLUNTEER_LISTING
                                )
                                {
                                    startActivity(
                                        Intent(
                                            this@VolunteerListingActivity,
                                            AddVolunteerActivity::class.java
                                        )
                                    )
                                }
                            )
                            return@Observer
                        }
                        tvCount.text = "${it.size} volunteers found"
                    }
                    it?.data?.data?.users?.let { users ->
                        val users1: ArrayList<User> = users as ArrayList<User>
                        adapter.updateContacts(users1)
                    }
                }
                Status.ERROR -> {
                    it.error?.value?.let {
                        Toast.makeText(this@VolunteerListingActivity, it, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
        addVolunteerViewModel.makeCall.observe(this, Observer {
            makeCall(it)
        })
        actionBack.setOnClickListener { onBackPressed() }
    }

    fun makeCall(value: String) {
        phone = value
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:${value}")
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.CALL_PHONE),
                102
            )
            return
        }
        startActivity(callIntent, null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        helper.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        helper.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // If request is cancelled, the result arrays are empty.
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 102) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                makeCall(phone)
            } else {
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
