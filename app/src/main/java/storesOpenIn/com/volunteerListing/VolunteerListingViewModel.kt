package storesOpenIn.com.volunteerListing

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.launch
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.utils.*
import java.math.RoundingMode
import java.text.DecimalFormat


/**
 * Created by BalaKrishnan
 */
class VolunteerListingViewModel(application: Application) : AndroidViewModel(application) {
    val gson: Gson = GsonBuilder().create()
    val responseData = MutableLiveData<Either<VolunteerListData>>()
    val makeCall = MutableLiveData<String>()
    private val TAG = javaClass.name
    fun getNearMeVolunteers(value: LatLong) {
        Log.d(TAG, "Location ${value.lat}, ${value.lng} ")
        val df = DecimalFormat("#.######")
        df.setRoundingMode(RoundingMode.CEILING)
        val query =
            "{\"query\":\"query storeDetails(\$locationGeo: geography!) {\n" +
                    "  users(where: {locationByLocation: {location: {_cast: {geography: {_st_d_within: {distance: 3000, from: \$locationGeo}}}}, users: {is_volunteer: {_eq: true}}}}) {\n" +
                    "    id\n" +
                    "    name\n" +
                    "    phone\n" +
                    "    location\n" +
                    "    locationByLocation {\n" +
                    "      location\n" +
                    "    }\n" +
                    "  }\n" +
                    "}\n\",\"variables\":{\"locationGeo\":{\"type\":\"Point\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"urn:ogc:def:crs:EPSG::4326\"}},\"coordinates\":[${df.format(value.lat)},${df.format(value.lng)}]}}}"

        viewModelScope.launch {

            val map = HashMap<String, String>()
            map["eC1oYXN1cmEtYWRtaW4tc2VjcmV0".decode()] = "UmVtZW1iZXIwMDE=".decode()

            val postValue = RetrofitUtil.getGraphQLAPI().postXValue(
                map = map,
                requestBody = query.getRequestBody()
            )

            val fromJson = gson.fromJson(postValue.string(), VolunteerListData::class.java)
            if (fromJson != null) {
                responseData.postValue(Either.success(fromJson))
            } else {
                responseData.postValue(Either.error(ApiError.SOMETHING_WENT_WRONG, null))
            }
        }
    }


    fun makePhoneCall(phone: String) {
        makeCall.postValue(phone)
    }

}