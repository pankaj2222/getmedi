package storesOpenIn.com.language.model

import java.io.Serializable

/**
 * Created by Pankaj Rajak on 28/05/2020.
 */
class Language(name: String, code: String) : Serializable {

    var name: String? = name
    var code: String? = code

    companion object {

        fun getLanguageList(): ArrayList<Language> {
            val list = ArrayList<Language>()
            list.add(Language("Select Language", "lang"))
            list.add(Language("English", "en"))
            list.add(Language("Hindi", "hi"))
            return list
        }
    }
}