package storesOpenIn.com.language

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.language.*
import storesOpenIn.com.R
import storesOpenIn.com.language.adapter.MyAdapter
import storesOpenIn.com.language.model.Language
import storesOpenIn.com.onboarding.OnboardingActivity
import storesOpenIn.com.onboarding.RegisterUserBottomDialogFragment
import storesOpenIn.com.onboarding.WelcomeActivity
import storesOpenIn.com.utils.executionRequireInternet
import java.util.*

@Suppress("DEPRECATION")
/**
 * Created by Pankaj Rajak on 28/05/2020.
 */
class LanguageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.language)

        setSpinner()

        btExit.setOnClickListener(View.OnClickListener {
            executionRequireInternet {
                startActivity(Intent(this, WelcomeActivity::class.java))
            }
            finish()
        })
    }

    private fun setSpinner() {
        val myAdapter = MyAdapter(this, Language.getLanguageList())
        spinner.adapter = myAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val language = adapterView.adapter.getItem(position) as Language
                if (position != 0) {
                    setLocale(language.code)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }
    }

    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res = resources
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, res.displayMetrics)
       // startActivity(Intent(this, LanguageActivity::class.java))
       // finish()
    }
}