package storesOpenIn.com
import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName
import storesOpenIn.com.storeListing.Store


/**
 * Created by BalaKrishnan
 */
@Keep
data class CheckStoreModel(
    @SerializedName("data")
    val `data`: Data = Data()
)

@Keep
data class Data(
    @SerializedName("stores")
    val stores: List<Store> = listOf()
)