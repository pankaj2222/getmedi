package storesOpenIn.com.storeListing

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_store_listing.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addStore.AddStoreActivity
import storesOpenIn.com.storeDetails.StoreDetailsActivity
import storesOpenIn.com.utils.*

class StoreListingActivity : AppCompatActivity(), StoresAdapter.OnItemClickListener {

    private lateinit var vm: StoreViewModel

    private var adapter: StoresAdapter? = null
    var fromProfile: Boolean = false
    val liveLocationHelper: LiveLocationHelper by lazy {
        LiveLocationHelper(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        liveLocationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_listing)

        executionOnInternet {
            liveLocationHelper.observe(this, Observer {

                if (it == null || it.isEmpty()) return@Observer

                vm = ViewModelFactory(
                    StoreVMData(
                        intent,
                        PreferenceHelper(this),
                        (application as BaseApplication).location
                    )
                ).create(StoreViewModel::class.java)
                adapter = StoresAdapter(intent.getIntExtra(KEY_ICON_OUTLINE, -1), fromProfile, it)
                adapter?.addItemClickListener(this)
                rvStores.layoutManager = LinearLayoutManager(this)
                rvStores.adapter = adapter
                bindObservers()
            })
        }

        setupUI()
    }

    private fun setupUI() {
        actionBack.setOnClickListener { onBackPressed() }
        actionLanguage.setOnClickListener {
            Toast.makeText(
                this,
                "Language clicked!",
                Toast.LENGTH_SHORT
            ).show()
        }


        actionAddStore.setOnClickListener {
            startActivity(Intent(this@StoreListingActivity, AddStoreActivity::class.java))
        }

        val title = intent.getStringExtra(KEY_NAME)
        val id = intent.getIntExtra(KEY_CATEGORY_ID, 0)
        fromProfile = intent.getBooleanExtra(KEY_FROM_PROFILE, false)
        val icon = intent.getIntExtra(KEY_ICON, -1)
        storeTitle.text = title

        if (fromProfile) {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            storeTitle.setTextColor(ContextCompat.getColor(this, R.color.colorTextPrimary))
            actionBack.setImageResource(R.drawable.ic_back_arrow_24)
            ivPattern.setVisible(false)
        } else {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            storeTitle.setTextColor(ContextCompat.getColor(this, android.R.color.white))
            actionBack.setColorFilter(ContextCompat.getColor(this, android.R.color.white));
            storeIcon.setImageDrawable(ContextCompat.getDrawable(this, icon))
        }
    }

    private fun bindObservers() {
        vm.stores.observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.isNullOrEmpty()) {
                labelItemsCount.visibility = View.GONE
                rvStores.visibility = View.GONE
                emptyView.visibility = View.VISIBLE
            } else {
                labelItemsCount.visibility = View.VISIBLE
                rvStores.visibility = View.VISIBLE
                emptyView.visibility = View.GONE

                val count = if (it.size < 10) "0${it.size}" else it.size
                labelItemsCount.text = "$count STORES FOUND"
                adapter?.addStores(it)
                adapter?.notifyDataSetChanged()
            }
        })
    }

    override fun onItemClicked(v: View, store: Store?) {
        if (v.id == R.id.btndelete) {
            Log.d("OnEditClick", "details: ${gson.toJson(store)}");
            val intent = Intent(this@StoreListingActivity, AddStoreActivity::class.java)
            intent.apply {
                putExtra(KEY_FROM_PROFILE, true)
                putExtra(KEY_STORE_DETAILS, store)
            }
            startActivity(intent)
        } else {
            val intent = Intent(this, StoreDetailsActivity::class.java)
            intent.putExtra(StoreDetailsActivity.KEY_STORE_VALUE, store)
            intent.putExtra(KEY_FROM_PROFILE, fromProfile)
            startActivity(intent)
        }
    }
}
