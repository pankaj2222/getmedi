package storesOpenIn.com.storeListing

import android.location.Location
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.shape.ShapeAppearanceModel
import kotlinx.android.synthetic.main.item_store_v2.view.*
import storesOpenIn.com.BaseApplication
import storesOpenIn.com.R
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.utils.setVisible

class StoresAdapter(
    private val storeIcon: Int,
    private val fromProfile: Boolean,
    val currentLocation: LatLong
) :
    RecyclerView.Adapter<StoresAdapter.ViewHolder>() {
    lateinit var storeHelpful: MutableLiveData<Store?>
    private var stores: MutableList<Store> = arrayListOf()
    fun addStores(stores: List<Store>) {
        this.stores.addAll(stores)
    }

    private var listener: OnItemClickListener? = null
    fun addItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_v2, parent, false)
        storeHelpful = ((view.context?.applicationContext) as BaseApplication).storeHelpful
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return stores.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(stores[position])
    }

    private fun distanceAway(storeLatLong: List<Double>?, textView: TextView) {
        if (storeLatLong != null && storeLatLong.isNotEmpty()) {
            if (currentLocation.isEmpty()) return
            val curLocation = Location("locationA")
            curLocation.latitude = currentLocation.lat
            curLocation.longitude = currentLocation.lng
            val storeLocation = Location("locationB")
            storeLocation.latitude =
                storeLatLong[0]
            storeLocation.longitude =
                storeLatLong[1]
            textView.text =
                (curLocation.distanceTo(storeLocation).toInt() / 1000).toString() + " km away"
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(store: Store?) {
            onHelpfulEditObserver(store)
            itemView.storeIcon.setImageResource(storeIcon)
            itemView.labelStoreName.text = store?.name
            distanceAway(store?.locationByLocation?.location?.coordinates, itemView.labelStatus)
            itemView.labelOrder.text = "  ${store?.helpful?.aggregate?.count.toString()} Helpful"
            itemView.labelDate.text =
                "  ${store?.notHelpful?.aggregate?.count.toString()} Not Helpful"
            itemView.setOnClickListener { listener?.onItemClicked(it, store) }
            (itemView.labelCategory as ChipGroup).removeAllViews()
            store?.storeXCategories?.forEach {
                addchip(itemView.labelCategory, it.category.name.capitalize())
            }
            itemView.btndelete.setOnClickListener {
                listener?.onItemClicked(it, store)
            }
            itemView.btnViewDetails.setOnClickListener {
                listener?.onItemClicked(it, store)
            }
            itemView.btndelete.setVisible(fromProfile)
            itemView.btnViewDetails.setVisible(fromProfile)
        }
    }

    private fun ViewHolder.onHelpfulEditObserver(store: Store?) {
        storeHelpful.observe((itemView.context as LifecycleOwner),
            Observer { chStore ->
                if (chStore == null || store == null) {
                    return@Observer
                }
                if (chStore.id == store.id) {
                    itemView.labelOrder.text =
                        "  ${chStore?.helpful?.aggregate?.count.toString()} Helpful"
                    itemView.labelDate.text =
                        "  ${chStore?.notHelpful?.aggregate?.count.toString()} Not Helpful"
                    storeHelpful.postValue(null)
                }
            })
    }

    private fun addchip(chipGroup: ChipGroup?, tagName: String): Chip? {
        if (chipGroup != null && chipGroup.context != null) {
            val chip = Chip(chipGroup.context)
            chip.setText(tagName, TextView.BufferType.EDITABLE)
            chip.id = View.generateViewId()
            chip.shapeAppearanceModel =
                ShapeAppearanceModel.builder().setAllCornerSizes(20f).build()
            chip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            chip.setBackgroundColor(ContextCompat.getColor(chipGroup.context, R.color.chip_color_bg))
            chip.checkedIcon = null
            chip.isCheckable = true
            chipGroup.addView(chip)
            return chip
        }
        return null

    }

    interface OnItemClickListener {
        fun onItemClicked(v: View, store: Store?)
    }
}