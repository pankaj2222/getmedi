package storesOpenIn.com.storeListing

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * Created by BalaKrishnan
 */
val gson = GsonBuilder().create()

@Keep
@Parcelize
data class Response(
    @SerializedName("data")
    val `data`: Data = Data()
) : Parcelable

@Keep
@Parcelize
data class Data(
    @SerializedName("stores")
    val stores: List<Store> = listOf()
) : Parcelable


@Keep
@Parcelize
data class Store(
    @SerializedName("helpful")
    val helpful: Helpful = Helpful(),
    @SerializedName("id")
    val id: Int = 0, // 10
    @SerializedName("description")
    val description: String = "", // description
    @SerializedName("locationByLocation")
    val locationByLocation: LocationByLocation = LocationByLocation(),
    @SerializedName("name")
    val name: String = "", // Store name
    @SerializedName("not_helpful")
    val notHelpful: NotHelpful = NotHelpful(),
    @SerializedName("photos")
    val photos: List<Photo> = listOf(),
    @SerializedName("store_x_categories")
    val storeXCategories: List<StoreXCategory> = listOf(),
    val user: User = User(),
    @SerializedName("helpfuls")
    val helpfuls: ArrayList<IsHelpful> = arrayListOf()

) : Parcelable {
    override fun toString(): String {
        return gson.toJson(this)
    }
}


@Keep
@Parcelize
data class User(
    val name: String = ""
) : Parcelable

@Keep
@Parcelize
data class Helpful(
    @SerializedName("aggregate")
    val aggregate: Aggregate = Aggregate()
) : Parcelable

@Keep
@Parcelize
data class Aggregate(
    @SerializedName("count")
    val count: Int = 0 // 0
) : Parcelable

@Keep
@Parcelize
data class LocationByLocation(
    @SerializedName("id")
    val id: Int = 0, // 1
    @SerializedName("location")
    val location: Location = Location()
) : Parcelable

@Keep
@Parcelize
data class Location(
    @SerializedName("coordinates")
    val coordinates: List<Double> = listOf(),
    @SerializedName("crs")
    val crs: Crs = Crs(),
    @SerializedName("type")
    val type: String = "" // Point
) : Parcelable

@Keep
@Parcelize
data class Crs(
    @SerializedName("properties")
    val properties: Properties = Properties(),
    @SerializedName("type")
    val type: String = "" // name
) : Parcelable

@Keep
@Parcelize
data class Properties(
    @SerializedName("name")
    val name: String = "" // urn:ogc:def:crs:EPSG::4326
) : Parcelable

@Keep
@Parcelize
data class NotHelpful(
    @SerializedName("aggregate")
    val aggregate: AggregateX = AggregateX()
) : Parcelable

@Keep
@Parcelize
data class AggregateX(
    @SerializedName("count")
    val count: Int = 0 // 0
) : Parcelable

@Keep
@Parcelize
data class Photo(
    @SerializedName("id")
    val id: Int = 0, // 2
    @SerializedName("url")
    val url: String = "" // photo_url
) : Parcelable

@Keep
@Parcelize
data class StoreXCategory(
    @SerializedName("category")
    val category: Category = Category()
) : Parcelable

@Keep
@Parcelize
data class Category(
    @SerializedName("id")
    val id: Int = 0, // 2
    @SerializedName("name")
    val name: String = "" // vegetables
) : Parcelable {
    override fun equals(other: Any?): Boolean {
        return if (other == Category::class.java) {
            (other as Category).id == this.id
        } else
            super.equals(other)
    }
}


@Keep
@Parcelize
data class IsHelpful(
    @SerializedName("is_helpful")
    val isHelpful: Boolean? = null
) : Parcelable