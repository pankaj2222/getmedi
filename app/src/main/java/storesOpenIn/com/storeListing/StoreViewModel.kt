package storesOpenIn.com.storeListing

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.utils.*

data class StoreVMData(
    val intent: Intent,
    val preferenceHelper: PreferenceHelper,
    val latLng: LatLong?
)

class StoreViewModel(storeVMData: StoreVMData) : ViewModel() {
    private val TAG = javaClass.name
    val stores: MutableLiveData<List<Store>> = MutableLiveData()
    val gson = GsonBuilder().create()
    val token =
        "Bearer " + storeVMData.preferenceHelper.user_token_id
    val categoryId = storeVMData.intent.getIntExtra(KEY_CATEGORY_ID, 0)

    init {
        viewModelScope.launch {
            val id = storeVMData.intent.getStringExtra(KEY_USER_ID)
            val postValue =
                RetrofitUtil.getGraphQLAPI()
                    .postValue(
                        value = token,
                        requestBody = if (!id.isNullOrEmpty())
                            queryUserStore(id)
                        else {
                            if (storeVMData.latLng != null) {
                                query(
                                    categoryId, 5, storeVMData.latLng.lat, storeVMData.latLng.lng
                                )
                            } else {
                                query(
                                    categoryId, 5
                                )
                            }
                        }
                    )

            val responseAsString = postValue.string()
            if (!responseAsString.isNullOrEmpty()) {
                val response: Response = gson.fromJson(responseAsString, Response::class.java)
                stores.postValue(response.data.stores)
            }

        }
    }

    fun query(categoryId: Int, radius: Int, lat: Double = 0.0, long: Double = 0.0): RequestBody {
//            "{\n" +
//                    "  \"query\": \"query storesInCategory(\$id: Int!) {\\nstores(where: {store_x_categories: {category_id: {_eq: \$id}, is_active: {_eq: true}}}, order_by: {location: asc}) {\\nid\\nname\\nstore_x_categories {\\ncategory {\\nid\\nname\\n}\\n}\\nlocationByLocation {\\nid\\nlocation\\n}\\nnot_helpful: helpfuls_aggregate(where: {is_helpful: {_eq: false}}) {\\naggregate {\\ncount\\n}\\n}\\nhelpful: helpfuls_aggregate(where: {is_helpful: {_eq: true}}) {\\naggregate {\\ncount\\n}\\n}\\n}\\n}\\n\",\n" +
//                    "  \"variables\": {\n" +
//                    "    \"id\": $categoryId\n" +
//                    "  }\n" +
//                    "}\n" +
//                    "\n"
        val payload =

            "{\n" +
                    "  \"query\": \"query (\$id: Int!, \$locationGeo: geography!,\$distance:Float!) {\\n  stores(where: {store_x_categories: {category_id: {_eq: \$id}, is_active: {_eq: true}}, locationByLocation: {location: {_cast: {geography: {_st_d_within: {distance: \$distance, from: \$locationGeo}}}}}}, order_by: {location: asc}) {\\n    id\\n    name\\n    store_x_categories {\\n      category {\\n        id\\n        name\\n      }\\n    }\\n    locationByLocation {\\n      id\\n      location\\n    }\\n    not_helpful: helpfuls_aggregate(where: {is_helpful: {_eq: false}}) {\\n      aggregate {\\n        count\\n      }\\n    }\\n    helpful: helpfuls_aggregate(where: {is_helpful: {_eq: true}}) {\\n      aggregate {\\n        count\\n      }\\n    }\\n  }\\n}\\n\",\n" +
                    "  \"variables\": {\n" +
                    "    \"id\": $categoryId,\n" +
                    "    \"locationGeo\": {\n" +
                    "      \"type\": \"Point\",\n" +
                    "      \"crs\": {\n" +
                    "        \"type\": \"name\",\n" +
                    "        \"properties\": {\n" +
                    "          \"name\": \"urn:ogc:def:crs:EPSG::4326\"\n" +
                    "        }\n" +
                    "      },\n" +
                    "      \"coordinates\": [\n" +
                    "        $lat,\n" +
                    "        $long\n" +
                    "      ]\n" +
                    "    },\n" +
                    "    \"distance\": ${radius*1000}\n" +
                    "  }\n" +
                    "}"
        return payload.getRequestBody()
    }

    fun queryUserStore(userID: String): RequestBody {
        val payload =
            "{\"query\":\"query storeByUser(\$user_id: String!) {\\n  stores(where: {user: {auth_id: {_eq: \$user_id}}}, order_by: {updated_at: asc}) {\\n    id\\n    name\\n    description\\n    locationByLocation {\\n      id\\n      location\\n    }\\n  store_x_categories {\\n      id\\n    category {\\n        id\\n        name\\n      }\\n      store {\\n        helpful: helpfuls_aggregate(where: {is_helpful: {_eq: true}}) {\\n          aggregate {\\n            count\\n          }\\n        }\\n   not_helpful: helpfuls_aggregate(where: {is_helpful: {_eq: false}}) {\\n          aggregate {\\n            count\\n          }\\n        }\\n      }\\n    }\\n  }\\n}\\n\",\"variables\":{\"user_id\":\"$userID\"}}"
        return payload.getRequestBody()
    }
}