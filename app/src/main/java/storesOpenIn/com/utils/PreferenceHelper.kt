package storesOpenIn.com.utils

import android.content.Context

/**
 * Created by BalaKrishnan
 */
const val NAME = "covidOpenIn"

/**
 * This PreferenceHelper will simplify the shared preference implementation
 *
 * Note: Please do not use camelCase. Since the name of the variable is used as key in shared preference
 */
class PreferenceHelper(context: Context) {
    private val TAG = javaClass.name
    private val sharedPreferences by lazy { context.getSharedPreferences(NAME, 0) }
    //Sample to store value as int
    var value: Int by sharedPreferences.int()
    //Sample to store value as string
    var sample_string : String by sharedPreferences.string()
    //Sample to store value as boolean
    var sample_boolean: Boolean by sharedPreferences.boolean()

    var user_phone_number : String by sharedPreferences.string()
    var user_email : String by sharedPreferences.string()
    var user_token_id : String by sharedPreferences.string()
    var user_name : String by sharedPreferences.string()

    var user_logged_in : Boolean by sharedPreferences.boolean()
}