package storesOpenIn.com.utils.location;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Jaison.
 */
public class LocationHelper {

    public static final int REQUEST_GPS_ACCESS_REQUEST_CODE = 111;
    public static final int REQUEST_LOCATION_REQUEST_CODE = 112;
    private static final String TAG = "LocationHelper";
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 222;
    private Activity activity;
    private MutableLiveData<Boolean> isLocationPermissionGranted;
    private MutableLiveData<Location> mLastKnownLocation;

    // To check the GPS permission and realtime location updates
    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback mLocationCallback;
    private AlertDialog alertDialog;

    private boolean isRealTimeUpdate = false;
    private long LOCATION_UPDATE_INTERVAL = 60000;

    public LocationHelper(Activity activity) {
        Log.d(TAG, "LocationHelper: ");
        this.activity = activity;
        init();
    }

    public LocationHelper(Activity activity, long LOCATION_UPDATE_INTERVAL) {
        this.activity = activity;
        this.isRealTimeUpdate = true;
        this.LOCATION_UPDATE_INTERVAL = LOCATION_UPDATE_INTERVAL;
        init();
    }

    private void init() {
        // To get the location permission status realtime
        isLocationPermissionGranted = new MutableLiveData<>();
        // To get the updated location
        mLastKnownLocation = new MutableLiveData<>();
        // To check the GPS is enabled/not
        locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        // To get the device location
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    public void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        Log.d(TAG, "getLocationPermission: ");
        if (ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "getLocationPermission: Granted");
            isLocationPermissionGranted.setValue(true);
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    public MutableLiveData<Boolean> observeLocationPermissionStatus() {
        return isLocationPermissionGranted;
    }

    public MutableLiveData<Location> observeLocation() {
        return mLastKnownLocation;
    }

    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: ");
        // If request is cancelled, the result arrays are empty.
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isLocationPermissionGranted.setValue(true);
            } else
                isLocationPermissionGranted.setValue(false);
        }
    }

    /*
     * Get the best and most recent location of the device, which may be null in rare
     * cases when a location is not available.
     */
    public void getDeviceLocation() {
        try {
            if (isLocationPermissionGranted.getValue() != null
                    && isLocationPermissionGranted.getValue()) {
                mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            mLastKnownLocation.setValue(location);
                            if (isRealTimeUpdate)
                                getLocationUpdates();
                            Log.d(TAG, "onSuccess: location not  null");
                            Log.d(TAG, "onComplete: Lat " + mLastKnownLocation.getValue().getLatitude());
                            Log.d(TAG, "onComplete: Long " + mLastKnownLocation.getValue().getLongitude());
                        } else {
                            Log.d(TAG, "onSuccess: location null");
                            checkGPSStatus();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * GPS isProviderEnabled sometimes return false because of the different locating method
     * (High Accuracy, Battery Saving & Device only)
     * <p>
     * To solve that issue, we are checking the provider contains gps or contains network
     */
    private boolean checkIfLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void checkGPSStatus() {
        if (checkIfLocationEnabled()) {
            Log.d(TAG, "checkGPSStatus: GPS is enabled");
            getLocationUpdates();
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        (dialog, id) -> {
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            activity.startActivityForResult(callGPSSettingIntent, REQUEST_GPS_ACCESS_REQUEST_CODE);
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                (dialog, id) -> dialog.cancel());
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode);
        if (requestCode == REQUEST_GPS_ACCESS_REQUEST_CODE && resultCode == 0) {
            if (checkIfLocationEnabled()) {
                Log.d(TAG, "onActivityResult: true");
                //Start searching for location
                getLocationUpdates();
            } else {
                // User did not switch on the GPS
                Log.d(TAG, "onActivityResult: false");

            }
        } else if (requestCode == REQUEST_LOCATION_REQUEST_CODE && resultCode == 0) {
            Log.d(TAG, "onActivityResult:" + REQUEST_LOCATION_REQUEST_CODE);
            getLocationPermission();
        }
    }

    private void getLocationUpdates() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        mLastKnownLocation.setValue(location);
                        Log.d(TAG, "getResults: location not  null");
                        Log.d(TAG, "getResults: Lat " + mLastKnownLocation.getValue().getLatitude());
                        Log.d(TAG, "getResults: Long " + mLastKnownLocation.getValue().getLongitude());

                        if (!isRealTimeUpdate) {
                            // To stop the location change updates.
                            stopLocationUpdates();
                        }
                    }
                }
            }
        };

        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
    }

    private void stopLocationUpdates() {
        if (mFusedLocationProviderClient != null && mLocationCallback != null)
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    public void onDestroy() {
        // To avoid window leaked issue

        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }

        if (mFusedLocationProviderClient != null && mLocationCallback != null)
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);

    }
}
