package storesOpenIn.com.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Base64
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresPermission
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import storesOpenIn.com.R
import storesOpenIn.com.dashboard.Item
import java.nio.charset.StandardCharsets

/**
 * Created by Pankaj
 */
fun View.setVisible(visible: Boolean) {
    this.visibility = if (visible)
        View.VISIBLE
    else View.GONE
}


fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

data class PopupData(
    val drawable: Int,
    val title: String,
    val desc: String,
    val addAnotherBtnString: String? = null,
    val shareBtnString: String,
    val clickListener: ClickListener? = null
) {
    interface ClickListener {
        fun onClick(v: View)
    }
}


fun View.showVolunteerPopup(data: PopupData): PopupWindow {
    val inflater =
        this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val popupView: View =
        inflater.inflate(R.layout.layout_success_state, null)

    val width = LinearLayout.LayoutParams.MATCH_PARENT
    val height = LinearLayout.LayoutParams.MATCH_PARENT
    val focusable =
        true // lets taps outside the popup also dismiss it

    popupView.findViewById<ImageView>(R.id.ivSuccessIcon).setImageDrawable(
        resources.getDrawable(data.drawable, null)
    )

    popupView.findViewById<TextView>(R.id.tvSucccessTitle).text = data.title
    popupView.findViewById<TextView>(R.id.tvSuccessDesc).text =
        data.desc
    val btnAnotherString = popupView.findViewById<MaterialButton>(R.id.btnSuccessAddAnother)

    val btnSuccessShare = popupView.findViewById<MaterialButton>(R.id.btnSuccessShare)

    if (data.addAnotherBtnString == null) {
        btnAnotherString.setVisible(false)
    } else
        btnAnotherString?.text = data.addAnotherBtnString

    btnSuccessShare.text = data.shareBtnString

    btnAnotherString.setOnClickListener {
        data.clickListener?.onClick(it)
    }

    btnSuccessShare.setOnClickListener {
        data.clickListener?.onClick(it)
    }

    val popupWindow =
        PopupWindow(popupView, width, height, focusable)

    popupView.findViewById<ImageView>(R.id.ivSuccessClose).setOnClickListener {
        popupWindow.dismiss()
    }


    popupWindow.setOnDismissListener {
        (this.context as Activity).finish()
    }
    this.post {
        popupWindow.showAtLocation(this, Gravity.CENTER, 0, 0)
    }
    return popupWindow

}

fun Activity.hideKeyBoard(view: View) {
    val methodManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    methodManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun Activity.showKeyboard(view: View) {
    val methodManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    methodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}

@Throws(JsonSyntaxException::class)
fun <T> String.fromJson(classOfT: Class<T>?): T? {
    val gson = GsonBuilder().create()
    return gson.fromJson(this, classOfT)
}

fun ChipGroup.addChip(tagName: String, backgroundRes: Int? = null, textRes: Int? = null): Chip? {
    if (this != null && this.context != null) {
        val chip = Chip(this.context)
        chip.setText(tagName, TextView.BufferType.EDITABLE)
        chip.id = View.generateViewId()
        chip.shapeAppearanceModel =
            ShapeAppearanceModel.builder().setAllCornerSizes(20f).build()
        chip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        chip.checkedIcon = null
        chip.isCheckable = true
        backgroundRes?.let {
            chip.setChipBackgroundColorResource(it)
        }
        textRes?.let {
            chip.setTextAppearanceResource(it)
        }
        this.addView(chip)
        return chip
    }
    return null

}

fun getCategories(itemsName: Array<String>): MutableList<Item> {
    val list: MutableList<Item> = arrayListOf()
    list.add(
        Item(
            itemsName[0],
            R.drawable.ic_grocery,
            R.drawable.ic_grocery_outline,
            false, 1
        )
    )
    list.add(
        Item(
            itemsName[1],
            R.drawable.ic_vegetables,
            R.drawable.ic_veg_outline,
            false, 2
        )
    )
    list.add(
        Item(
            itemsName[2],
            R.drawable.ic_medicine,
            R.drawable.ic_medicine_outline,
            false, 3
        )
    )
    list.add(
        Item(
            itemsName[3],
            R.drawable.ic_bread,
            R.drawable.ic_dairybread_outline,
            false, 4
        )
    )
    list.add(
        Item(
            itemsName[4],
            R.drawable.ic_fruits,
            R.drawable.ic_fruits_outline,
            false, 5
        )
    )

    list.add(
        Item(
            itemsName[5],
            R.drawable.ic_others,
            R.drawable.ic_others_outline,
            false, 6
        )
    )

    return list
}

fun Activity.shareApp(shareText: String) {
    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, shareText)
        type = "text/plain"
    }
    val shareIntent = Intent.createChooser(sendIntent, null)
    this.startActivity(shareIntent)
}

fun String.decode(): String =
    String(
        Base64.decode(this, Base64.DEFAULT),
        StandardCharsets.UTF_8
    )

@RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
fun Context.isOnline(): Boolean {
    val connectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (connectivityManager != null) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> return true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> return true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> return true
                }
            }
        } else {
            connectivityManager.activeNetworkInfo.isConnected
        }
    }
    return false
}

fun View.snack(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).show()
}

fun Activity.showAlert(title: String? = null, message: String) {
    val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
    title?.let {
        alertDialog.setTitle(it)
    }
    alertDialog.setMessage(message)
    alertDialog.setButton(
        AlertDialog.BUTTON_POSITIVE, "Ok"
    ) { dialog, _ -> dialog?.dismiss() }
    alertDialog.show()
}

fun Activity.executionRequireInternet(doSomething: () -> Unit) {
    if (isOnline()) {
        doSomething()
    } else {
        showAlert(message = getString(R.string.internet_check))
    }
}

fun Activity.executionOnInternet(doSomething: () -> Unit) {
    if (isOnline()) {
        doSomething()
    } else {
        var canExecuted = true
        val connectionStateMonitor = ConnectionStateMonitor(this)
        connectionStateMonitor.observe(this as LifecycleOwner, Observer {
            it?.let {
                if (it && canExecuted) {
                    doSomething()
                    canExecuted = false
                }
            }
        })
        showAlert(message = getString(R.string.internet_check))
    }
}
