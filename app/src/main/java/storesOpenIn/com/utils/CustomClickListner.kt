package storesOpenIn.com.utils

import android.view.View

interface CustomClickListner<T> {
    fun onClick(v: View, data: T)
}