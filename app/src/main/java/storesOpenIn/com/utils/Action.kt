package storesOpenIn.com.utils

enum class Action {
    PLACE_SELECTION_ADDED,
    LOADING,
    LOADING_DONE,
    VOLUNTEER_ADDED,
    VOLUNTEER_REMOVED,
    ERROR,
    RECEIVED_STORE_DETAILS,
    HELP_DATA;
}

data class Navigation(
    val action: Action,
    val param: Any?
)