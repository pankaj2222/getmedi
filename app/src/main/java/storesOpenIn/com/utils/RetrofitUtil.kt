package storesOpenIn.com.utils

import android.util.Base64
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import storesOpenIn.com.BuildConfig
import java.nio.charset.StandardCharsets

/**
 * Created by BalaKrishnan
 */
object RetrofitUtil {
    private val TAG = javaClass.name

    var R_INSTANCE: Retrofit? = null;

    fun provideRetrofit(): Retrofit {
        return if (R_INSTANCE == null) {
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideOkHttpClient())
                .build()
        } else
            R_INSTANCE!!

    }

    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    fun provideOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(provideLoggingInterceptor())
        return httpClient.build()
    }

    fun getGraphQLAPI() =
        provideRetrofit().create(GraphQL::class.java)


    interface GraphQL {
        @POST
        suspend fun postValue(
            @Url url: String = "",
            @Header("Authorization") value: String = "",
            @Body requestBody: RequestBody
        ): ResponseBody

        @POST
        suspend fun postXValue(
            @Url url: String = "",
            @HeaderMap map:Map<String,String>,
            @Body requestBody: RequestBody
        ): ResponseBody

        @POST
        fun jpostValue(
            @Url url: String = "",
            @Header("Authorization") value: String = "",
            @Body requestBody: RequestBody
        ): Call<ResponseBody>
    }

}

fun String.getRequestBody(): RequestBody {
    return this.toRequestBody("application/json".toMediaTypeOrNull())
}


