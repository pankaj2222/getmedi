package storesOpenIn.com.utils

import androidx.annotation.Keep

@Keep
data class Either<out T>(val status: Status, val data: T?, val error: ApiError?) {

    companion object {
        fun <T> success(data: T?): Either<T> {
            return Either(Status.SUCCESS, data, null)
        }

        fun <T> error(error: ApiError, data: T?): Either<T> {
            return Either(Status.ERROR, data, error)
        }
    }
}

@Keep
enum class Status {
    SUCCESS,
    ERROR
}

@Keep
enum class ApiError(val value: String) {
    SOMETHING_WENT_WRONG("Something went wrong")

}