package storesOpenIn.com.utils

import okhttp3.RequestBody
import org.json.JSONArray

/**
 * Created by BalaKrishnan
 */
object APIQuery {
    fun getUserDetails() =
        "{\"query\":\"query Users {\\n  users{\\n    name,\\n    phone,\\n    location,\\n    auth_id,\\n    is_volunteer\\n  }\\n}\\n\",\"variables\":{}}".getRequestBody()

    fun getLocationDetails(locationId: Int): RequestBody {
        val t = "{\n" +
                "  \"query\": \"query getLocationById(\$locationID: Int!) {\\n  location(where: {id: {_eq: \$locationID}}) {\\n    location\\n  }\\n}\\n\",\n" +
                "  \"variables\": {\n" +
                "    \"locationID\": $locationId\n" +
                "  }\n" +
                "}"
        return t.getRequestBody()
    }

    fun updateUserWithPhoneNumber(
        name: String,
        locationId: Int,
        isVolunteer: Boolean
    ): RequestBody {
        val payload =
            "{\n" +
                    "  \"query\": \"mutation updateUserDetails(\$name: String, \$location_id: Int, \$is_volunteer: Boolean) {\\n  update_users(where: {},\\n    _set: {\\n      name: \$name,\\n    location: \$location_id,\\n      is_volunteer: \$is_volunteer\\n  }) {\\n   returning {\\n       id\\n   }\\n  }\\n}\",\n" +
                    "  \"variables\": {\n" +
                    "    \"name\": \"$name\",\n" +
                    "    \"location_id\": $locationId,\n" +
                    "    \"is_volunteer\": $isVolunteer\n" +
                    "  }\n" +
                    "}"
        return payload.getRequestBody()
    }

    fun updateUserWOPhoneNumber(
        name: String,
        phoneNumber: String,
        locationId: Int,
        isVolunteer: Boolean
    ): RequestBody {
        val payload =
            "{\n" +
                    "  \"query\": \"mutation updateUserDetails(\$name: String, \$phone: String, \$location_id: Int, \$is_volunteer: Boolean) {\\n  update_users(where: {},\\n    _set: {\\n      name: \$name,\\n      phone: \$phone,\\n      location: \$location_id,\\n      is_volunteer: \$is_volunteer\\n  }) {\\n   returning {\\n       id\\n   }\\n  }\\n}\",\n" +
                    "  \"variables\": {\n" +
                    "    \"name\": \"$name\",\n" +
                    "    \"phone\": \"$phoneNumber\",\n" +
                    "    \"location_id\": $locationId,\n" +
                    "    \"is_volunteer\": $isVolunteer\n" +
                    "  }\n" +
                    "}"
        return payload.getRequestBody()
    }

    fun updateVolunteer(isVolunteer: Boolean): RequestBody {
        val payload =
            "{\n" +
                    "  \"query\": \"mutation updateUserDetails( \$is_volunteer: Boolean) {\\n  update_users(where: {},\\n    _set: {\\n      is_volunteer: \$is_volunteer\\n  }) {\\n   returning {\\n       id\\n   }\\n  }\\n}\",\n" +
                    "  \"variables\": {\n" +
                    "    \"is_volunteer\": $isVolunteer\n" +
                    "  }\n" +
                    "}"
        return payload.getRequestBody()
    }

    fun checkStoreQuery(name: String, lat: Double, long: Double): RequestBody {
        val payload = "{\n" +
                "  \"query\": \"query MyQuery(\$name: String!, \$location: geometry!) {\\n  stores(where: {name: {_eq: \$name}, _and: {locationByLocation: {location: {_eq: \$location}}}}) {\\n    id\\n  }\\n}\",\n" +
                "  \"variables\": {\n" +
                "    \"name\": \"$name\",\n" +
                "    \"location\": {\n" +
                "      \"type\": \"Point\",\n" +
                "      \"crs\": {\n" +
                "        \"type\": \"name\",\n" +
                "        \"properties\": {\n" +
                "          \"name\": \"urn:ogc:def:crs:EPSG::4326\"\n" +
                "        }\n" +
                "      },\n" +
                "      \"coordinates\": [\n" +
                "        $lat,\n" +
                "        $long\n" +
                "      ]\n" +
                "    }\n" +
                "  }\n" +
                "}"
        return payload.getRequestBody()
    }

    fun insertStoreQuery(
        name: String,
        desc: String,
        category: JSONArray,
        lat: Double,
        lng: Double
    ): RequestBody {
        val s = "{\n" +
                "  \"query\": \"mutation insertStore(\\n  \$location: geometry!,\\n  \$categories: [store_x_category_insert_input!]!,\\n  \$name: String!,\\n  \$description: String!\\n ) {\\n  insert_stores(objects: {\\n    name: \$name,\\n    description: \$description,\\n  \\n    store_x_categories: { data: \$categories },\\n    locationByLocation: {\\n      data: { location: \$location },\\n      on_conflict: { constraint: location_location_key, update_columns: updated_at }\\n    }\\n  }) {\\n    returning {\n" + "      id\n" + "    } affected_rows\\n  }\\n}\\n\\n\",\n" +
                "  \"variables\": {\n" +
                "    \"location\": {\n" +
                "      \"type\": \"Point\",\n" +
                "      \"crs\": {\n" +
                "        \"type\": \"name\",\n" +
                "        \"properties\": {\n" +
                "          \"name\": \"urn:ogc:def:crs:EPSG::4326\"\n" +
                "        }\n" +
                "      },\n" +
                "      \"coordinates\": [\n" +
                "        $lat,\n" +
                "        $lng\n" +
                "      ]\n" +
                "    },\n" +
                "    \"categories\":" + category + "," +
                "    \"name\": \"$name\",\n" +
                "    \"description\": \"$desc\"\n" +
                "  }\n" +
                "}"
        return s.getRequestBody()
    }

    fun updateStoreQuery(
        name: String,
        desc: String,
        store_id: Int,
        locationId: Int
    ): RequestBody {

        val s = "{\n" +
                "\"query\": \"mutation updateStore(\$store_id: Int!, \$name: String!, \$location: Int!, \$description: String!) {\\n  update_stores(where: {id: {_eq: \$store_id}}, _set: {name: \$name, location: \$location, description: \$description}) {\\n    affected_rows\\n  }\\n}\"," +
                "  \"variables\": {\n" +
                "    \"store_id\": $store_id,\n" +
                "    \"name\": \"$name\",\n" +
                "    \"description\": \"$desc\",\n" +
                "    \"location\": $locationId\n" +
                "  }\n" +
                "}"
        return s.getRequestBody()
    }

    fun insertCategoryQuery(jsonArray: JSONArray): RequestBody {
        val payload = "{\n" +
                "  \"query\": \"mutation updateStoreCategories(\$obj: [store_x_category_insert_input!]!) {\\n  insert_store_x_category(on_conflict: {constraint: store_x_category_pkey, update_columns: category_id}, objects: \$obj) {\\n    affected_rows\\n  }\\n}\\n\",\n" +
                "  \"variables\": {\n" +
                "    \"obj\":$jsonArray \n" +
                "  }\n" +
                "}"
        return payload.getRequestBody()
    }

    fun deleteCategoryQuery(store_id: Int): RequestBody {
        val payload = "{\n" +
                "  \"query\": \"mutation deleteStoreXCategories (\$store_id:Int!) {\\n  delete_store_x_category(where: {store_id: {_eq: \$store_id}}) {\\n    affected_rows\\n  }\\n}\",\n" +
                "  \"variables\": {\n" +
                "    \"store_id\": $store_id\n" +
                "  }\n" +
                "}"
        return payload.getRequestBody()
    }

    fun upsertLocationQuery(lat: Double, long: Double): RequestBody {
        val payload =
            "{\"query\":\"mutation upsertLocation {\\n  insert_location(objects: {location:{\\n    type: \\\"Point\\\",\\n    crs: {\\n        type: \\\"name\\\",\\n        properties: {\\n            name: \\\"urn:ogc:def:crs:EPSG::4326\\\"\\n        }\\n    },\\n    coordinates: [\\n        $lat,\\n        $long\\n    ]\\n  } }, on_conflict: {constraint: location_location_key, update_columns: updated_at}) {\\n    returning {\\n      id\\n    }\\n  }\\n}\",\"variables\":{}}";
        return payload.getRequestBody()
    }

    fun updateUsername(name: String): RequestBody {
        val payload = "{\n" +
                "  \"query\": \"mutation updateStore(\$name: String!) {\\n  update_users(_set: {name: \$name},where : {}){\\n    returning{\\n      id\\n    }\\n  }\\n}\\n\",\n" +
                "  \"variables\": {\n" +
                "    \"name\": \"$name\"\n" +
                "  }\n" +
                "}"
        return payload.getRequestBody()
    }
    fun deleteStore(storeId: Int): RequestBody {
        val payload = "{\n" +
                "  \"query\": \"mutation updateStoreCategories(\$storeID: Int!) {\\n  delete_store_x_category(where :{store_id:{_eq:\$storeID}}){\\n    affected_rows\\n  }\\n  delete_stores(where: {id: {_eq: \$storeID}}) {\\n    affected_rows\\n  }\\n}\\n\",\n" +
                "  \"variables\" : {\"storeID\":$storeId}\n" +
                "}"
        return payload.getRequestBody()
    }
}