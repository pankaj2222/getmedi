package storesOpenIn.com.utils

const val KEY_NAME = "NAME"
const val KEY_FROM_PROFILE = "KEY_FROM_PROFILE"
const val KEY_USER_ID = "KEY_USER_ID"
const val KEY_STORE_ID = "KEY_STORE_ID"
const val KEY_STORE_DETAILS = "KEY_STORE_DETAILS"
const val KEY_CATEGORY_ID = "ID"
const val KEY_ICON = "ICON"
const val KEY_ICON_OUTLINE = "OUTLINE"

//const used in Volunteer Flow
const val KEY_LOCATION = "LOCATION"

const val NET_BHRAMA_HELP_MAIL = "helpmate@netbramha.com"
