package storesOpenIn.com.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Looper
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.google.android.gms.location.*
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import storesOpenIn.com.dashboard.DashboardActivity

/**
 * Created by BalaKrishnan
 */
class LiveLocationHelper(val activity: Activity) : LiveData<LatLong>() {
    private val locationPermissionRequest = 123

    var mFusedLocationClient: FusedLocationProviderClient? = null

    init {
        getLastLocation()
    }

    private fun getLastLocation() {
        if (checkLocationPermissions()) {
            if (isLocationEnabled()) {
                if (mFusedLocationClient == null)
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
                mFusedLocationClient?.lastLocation?.addOnCompleteListener { task ->
                    val location: Location? = task.result
                    onLocationReceived(location)
                }
            } else {
                val intent =
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                activity.startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun requestNewLocationData() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location? = locationResult.lastLocation
            onLocationReceived(location)
        }
    }

    private fun onLocationReceived(location: Location?) {
        location?.let { value ->
            val latLong = LatLong(value.latitude, value.longitude)
            setValue(latLong)
        }
        if (location == null) {
            requestNewLocationData()
        } else {
            DashboardActivity.mLastLocation = location
        }
    }

    private fun checkLocationPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            activity,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            locationPermissionRequest
        )
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager =
            activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            locationPermissionRequest -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            activity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        getLastLocation()
                    }
                } else {
                    val builder = AlertDialog.Builder(activity)
                    builder.setMessage("Location access is required for browsing nearby stores.")
                        .setPositiveButton(
                            "OPEN SETTINGS"
                        ) { dialog, _ ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", activity.packageName, null)
                            intent.data = uri
                            activity.startActivity(intent)
                            dialog.dismiss()
                        }
                    builder.create().show()
                }
                return
            }
        }
    }

}