package storesOpenIn.com.utils

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import storesOpenIn.com.addVolunteer.AddVolunteerVMData
import storesOpenIn.com.addVolunteer.AddVolunteerViewModel
import storesOpenIn.com.profile.ProfileVMData
import storesOpenIn.com.profile.ProfileViewModel
import storesOpenIn.com.dashboard.DashboardVMData
import storesOpenIn.com.dashboard.DashboardViewModel
import storesOpenIn.com.storeListing.StoreVMData
import storesOpenIn.com.storeListing.StoreViewModel
import storesOpenIn.com.storeDetails.StoreDetailsVMData
import storesOpenIn.com.storeDetails.StoreDetailsViewModel


/**
 * Created by BalaKrishnan
 */

@Suppress("UNCHECKED_CAST")
class ViewModelFactory<in Y>(private val repository: Y) :
    ViewModelProvider.Factory {
    private val TAG = javaClass.name
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            StoreViewModel::class.java -> {
                Log.d(TAG, " StoreViewModel ")
                StoreViewModel(repository as StoreVMData) as T
            }
            StoreDetailsViewModel::class.java -> {
                StoreDetailsViewModel(repository as StoreDetailsVMData) as T
            }
            ProfileViewModel::class.java ->{
                ProfileViewModel(repository as ProfileVMData) as T
            }
            AddVolunteerViewModel::class.java ->{
                AddVolunteerViewModel(repository as AddVolunteerVMData) as T
            }
            DashboardViewModel::class.java->{
                DashboardViewModel(repository as DashboardVMData) as T
            }
            else -> {
                Log.d(TAG, " Null ")
                null as T
            }
        }
    }

}

