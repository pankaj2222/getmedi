package storesOpenIn.com.utils.emptyState;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import storesOpenIn.com.R;
import storesOpenIn.com.utils.emptyState.EmptyStateData;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * A  class created for the related utils functions of EmptyState
 * Created by Jaison.
 */

public class EmptyStateHelper {

    private static final String TAG = "EmptyStateHelper";
    Context context;
    TextView tvEmptyStateDesc, tvTile;
    MaterialButton btnCTA;
    ImageView ivplaceHolder;

    public EmptyStateHelper(Context context) {
        this.context = context;
    }


    /**
     * A method to show the EmptyState for given FrameLayout
     *
     * @param frameLayout    a param has the frameLayout in which the EmptyState has to set
     * @param emptyStateData a param has the Value of the model of the EmptyStateData
     */
    public void showEmptyState(FrameLayout frameLayout, EmptyStateData emptyStateData) {
        // remove all the child views
        Gson gson = new GsonBuilder().create();
        Log.d(TAG, "showEmptyState: "+ gson.toJson(emptyStateData));
        if (frameLayout.getChildCount() > 0)
            frameLayout.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View childLayout = inflater.inflate(R.layout.layout_empty_state, null);
        tvEmptyStateDesc = childLayout.findViewById(R.id.tvDescription);
        if (TextUtils.isEmpty(emptyStateData.getDetailsText()))
            tvEmptyStateDesc.setVisibility(View.GONE);
        else
            tvEmptyStateDesc.setText(emptyStateData.getDetailsText());

        tvTile = childLayout.findViewById(R.id.tvTitle);
        if (TextUtils.isEmpty(emptyStateData.getHeaderText()))
            tvTile.setVisibility(View.GONE);
        else
            tvTile.setText(emptyStateData.getHeaderText());

        btnCTA = childLayout.findViewById(R.id.btnAction);
        if (TextUtils.isEmpty(emptyStateData.getCtaString()))
            btnCTA.setVisibility(View.GONE);
        else {
            btnCTA.setText(emptyStateData.getCtaString());
            if (emptyStateData.getCtaListener() != null)
                btnCTA.setOnClickListener(v -> emptyStateData.getCtaListener().onClick(v));
            if (emptyStateData.getCtaIcon() != 0) {
                btnCTA.setIcon(ContextCompat.getDrawable(btnCTA.getContext(), emptyStateData.getCtaIcon()));
            }
        }

        ivplaceHolder = childLayout.findViewById(R.id.ivPlaceHolder);
        if (emptyStateData.getDrawable() == 0)
            ivplaceHolder.setVisibility(View.GONE);
        else
            ivplaceHolder.setImageDrawable(ContextCompat.getDrawable(ivplaceHolder.getContext(), emptyStateData.getDrawable()));


        frameLayout.addView(childLayout);


        if (frameLayout.getVisibility() == View.INVISIBLE || frameLayout.getVisibility() == View.GONE) {
            Log.d(TAG, "showEmptyState: ");
            frameLayout.setVisibility(View.VISIBLE);
        } else
            Log.d(TAG, "showEmptyState: already showing");

    }

    /**
     * A method to hide the EmptyState
     *
     * @param frameLayout a param has the frameLayout in which the EmptyState is already  set
     */
    public void hideEmptyState(FrameLayout frameLayout) {
        if (frameLayout.getVisibility() == View.VISIBLE)
            frameLayout.setVisibility(View.GONE);
    }

}
