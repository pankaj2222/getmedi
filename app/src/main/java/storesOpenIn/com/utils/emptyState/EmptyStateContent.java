package storesOpenIn.com.utils.emptyState;

import android.content.Context;

import storesOpenIn.com.R;
import storesOpenIn.com.utils.emptyState.CTAListener;
import storesOpenIn.com.utils.emptyState.EmptyStateData;


/**
 * A Enum class to create EmptyStateContent types
 * Created by Jaison.
 */
public enum EmptyStateContent {
    NO_INTERNET, EMPTY_STATE_VOLUNTEER_LISTING,EMPTY_STATE_VOLUNTEER_LISTING_NOCTA;

    /**
     * A method is created to get the EmptyStateData
     *
     * @param context a param has a context of teh current activity
     * @param content a param has the EmptyStateContent enum type
     * @return it returns the EmptyStateData  based on the  EmptyStateContent enum type
     */

    public static storesOpenIn.com.utils.emptyState.EmptyStateData getData(Context context, EmptyStateContent content) {
        storesOpenIn.com.utils.emptyState.EmptyStateData emptyStateData = new storesOpenIn.com.utils.emptyState.EmptyStateData();

        switch (content) {
            case NO_INTERNET:
                emptyStateData.setHeaderText(context.getResources().getString(R.string.no_internet));
                emptyStateData.setDetailsText(context.getResources().getString(R.string.no_internet_desc));
                emptyStateData.setDrawable(R.drawable.ic_add_location);
                emptyStateData.setCtaString(context.getResources().getString(R.string.tryagain));
                emptyStateData.setEnableSwipeRefesh(true);
                return emptyStateData;
        }

        return emptyStateData;
    }

    public static storesOpenIn.com.utils.emptyState.EmptyStateData getData(Context context, EmptyStateContent content, CTAListener ctaListener) {
        storesOpenIn.com.utils.emptyState.EmptyStateData emptyStateData = new EmptyStateData();
        switch (content) {
            case EMPTY_STATE_VOLUNTEER_LISTING:
                emptyStateData.setHeaderText(context.getResources().getString(R.string.no_volunteer_found));
                emptyStateData.setDetailsText(context.getResources().getString(R.string.no_volunteer_found_desc));
                emptyStateData.setDrawable(R.drawable.es_img_volunteer);
                emptyStateData.setCtaIcon(R.drawable.ic_add_person);
                emptyStateData.setCtaListener(ctaListener);
                emptyStateData.setCtaString(context.getResources().getString(R.string.text_add_volunteer));
                emptyStateData.setEnableSwipeRefesh(true);
                return emptyStateData;
            case EMPTY_STATE_VOLUNTEER_LISTING_NOCTA:
                emptyStateData.setHeaderText(context.getResources().getString(R.string.no_volunteer_found));
                emptyStateData.setDetailsText(context.getResources().getString(R.string.no_volunteer_found_desc));
                emptyStateData.setDrawable(R.drawable.es_img_volunteer);
                emptyStateData.setCtaIcon(R.drawable.ic_add_person);
                emptyStateData.setCtaListener(ctaListener);
                emptyStateData.setEnableSwipeRefesh(true);
                return emptyStateData;

        }

        return emptyStateData;
    }
}
