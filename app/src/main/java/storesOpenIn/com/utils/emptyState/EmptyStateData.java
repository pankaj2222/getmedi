package storesOpenIn.com.utils.emptyState;

import storesOpenIn.com.utils.emptyState.CTAListener;

public class EmptyStateData {
    String headerText;
    String detailsText;
    String ctaString;
    int drawable = 0;
    int ctaIcon = 0;
    boolean isEnableSwipeRefesh;
    storesOpenIn.com.utils.emptyState.CTAListener ctaListener;

    public int getCtaIcon() {
        return ctaIcon;
    }

    public void setCtaIcon(int ctaIcon) {
        this.ctaIcon = ctaIcon;
    }

    public EmptyStateData(String headerText, String detailsText, String ctaString, int drawable, boolean isEnableSwipeRefesh, storesOpenIn.com.utils.emptyState.CTAListener ctaListener, int ctaIcon) {
        this.headerText = headerText;
        this.detailsText = detailsText;
        this.ctaString = ctaString;
        this.drawable = drawable;
        this.isEnableSwipeRefesh = isEnableSwipeRefesh;
        this.ctaListener = ctaListener;
        this.ctaIcon = ctaIcon;
    }

    public EmptyStateData(String headerText, String detailsText, String ctaString, int drawable) {
        this.headerText = headerText;
        this.detailsText = detailsText;
        this.drawable = drawable;
        this.ctaString = ctaString;
    }

    public EmptyStateData() {
    }

    public String getCtaString() {
        return ctaString;
    }

    public void setCtaString(String ctaString) {
        this.ctaString = ctaString;
    }

    public storesOpenIn.com.utils.emptyState.CTAListener getCtaListener() {
        return ctaListener;
    }

    public void setCtaListener(CTAListener ctaListener) {
        this.ctaListener = ctaListener;
    }

    /**
     * A method to get the Header Text of the Empty State
     *
     * @return it returns the Header Text of the Empty State
     */
    public String getHeaderText() {
        return headerText;
    }

    /**
     * A method to set the Header Text of the Empty State
     *
     * @param headerText a param has the value of the Header Text of EmptyState
     */
    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    /**
     * A method to get the Details Text of the Empty State
     *
     * @return it returns the Details Text of the Empty State
     */
    public String getDetailsText() {
        return detailsText;
    }

    /**
     * A method to set the Details Text of the Empty State
     *
     * @param detailsText a param has the value of the Details Text of EmptyState
     */
    public void setDetailsText(String detailsText) {
        this.detailsText = detailsText;
    }

    /**
     * A method to get the Drawable Image of the Empty State
     *
     * @return it returns the drawable for EmptyState Image
     */
    public int getDrawable() {
        return drawable;
    }

    /**
     * A method to set the Drawable Image of the Empty State
     *
     * @param drawable a param has the drawable for EmptyState Image
     */
    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    /**
     * to get the value of the Swipe Refresh is Enable for the Current Particular EmptyState
     *
     * @return it returns true if EmptyState has swipeRefresh Enabled
     */
    public boolean isEnableSwipeRefesh() {
        return isEnableSwipeRefesh;
    }

    /**
     * to set the value of the Swipe Refresh  is whether  Enable or not  for the Current Particular EmptyState
     *
     * @param enableSwipeRefesh a param has the boolean value of SwipeRefresh enable
     */
    public void setEnableSwipeRefesh(boolean enableSwipeRefesh) {
        isEnableSwipeRefesh = enableSwipeRefesh;
    }
}
