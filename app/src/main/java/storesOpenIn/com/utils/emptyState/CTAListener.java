package storesOpenIn.com.utils.emptyState;

import android.view.View;

/**
 * Created by BalaKrishnan
 */
public interface CTAListener {
    public void onClick(View v);
}
