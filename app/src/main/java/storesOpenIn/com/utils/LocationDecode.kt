package storesOpenIn.com.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong
import java.util.*

/**
 * Created by BalaKrishnan
 */
object LocationDecode {
    fun decodeGeo(context: Context, location: LatLong): String {
        try {
            val myLocation = Geocoder(context, Locale.getDefault())
            val myList: List<Address> =
                myLocation.getFromLocation(
                    location.lat
                    , location.lng, 1
                )
            if (myList.isEmpty()) return ""
            val address: Address = myList[0]
            var addressStr = ""
            if (!address.getAddressLine(0).isNullOrEmpty())
                addressStr += address.getAddressLine(0)?.toString() + ", "
            if (!address.getAddressLine(1).isNullOrEmpty())
                addressStr += address.getAddressLine(1)?.toString() + ", "
            if (!address.getAddressLine(2).isNullOrEmpty())
                addressStr += address.getAddressLine(2)
            return addressStr
        } catch (e: Exception) {
            return ""
        }
    }

    fun getAddress(context: Context, location: LatLong): Address {
        try {
            val myLocation = Geocoder(context, Locale.getDefault())
            val myList: List<Address> =
                myLocation.getFromLocation(
                    location.lat
                    , location.lng, 1
                )

            return if (myList.isEmpty())
                Address(Locale.getDefault())
            else
                myList[0]
        } catch (e: Exception) {
            return Address(Locale.getDefault())
        }
    }

}