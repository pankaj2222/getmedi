package storesOpenIn.com.addStore
import androidx.annotation.Keep


import com.google.gson.annotations.SerializedName



/**
 * Created by BalaKrishnan
 */
@Keep
data class AddStoreData(
    @SerializedName("data")
    val `data`: Data = Data()
)

@Keep
data class Data(
    @SerializedName("insert_stores")
    val insertStores: InsertStores = InsertStores()
)

@Keep
data class InsertStores(
    @SerializedName("affected_rows")
    val affectedRows: Int = 0, // 3
    @SerializedName("returning")
    val returning: List<Returning> = listOf()
)

@Keep
data class Returning(
    @SerializedName("id")
    val id: Int = 0 // 32
)

@Keep
data class DeleteResponse(
    @SerializedName("data")
    val `data`: DeleteData = DeleteData()
)

@Keep
data class DeleteData(
    @SerializedName("delete_store_x_category")
    val deleteStoreXCategory: DeleteStoreXCategory = DeleteStoreXCategory(),
    @SerializedName("delete_stores")
    val deleteStores: DeleteStores = DeleteStores()
)

@Keep
data class DeleteStoreXCategory(
    @SerializedName("affected_rows")
    val affectedRows: Int = 0 // 2
)

@Keep
data class DeleteStores(
    @SerializedName("affected_rows")
    val affectedRows: Int = 0 // 1
)