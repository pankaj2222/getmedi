package storesOpenIn.com.addStore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import storesOpenIn.com.CheckStoreModel;
import storesOpenIn.com.R;
import storesOpenIn.com.addVolunteer.data.UpsertLocationResponse;
import storesOpenIn.com.addVolunteer.placeSelection.PlaceSelectionActivity;
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLong;
import storesOpenIn.com.addVolunteer.placeSelection.data.LatLongWrapper;
import storesOpenIn.com.dashboard.Item;
import storesOpenIn.com.storeDetails.StoreDetailsActivity;
import storesOpenIn.com.storeListing.Helpful;
import storesOpenIn.com.storeListing.LocationByLocation;
import storesOpenIn.com.storeListing.NotHelpful;
import storesOpenIn.com.storeListing.Store;
import storesOpenIn.com.storeListing.StoreXCategory;
import storesOpenIn.com.storeListing.User;
import storesOpenIn.com.utils.APIQuery;
import storesOpenIn.com.utils.AnchorSheetBehavior;
import storesOpenIn.com.utils.CommonExtensionsKt;
import storesOpenIn.com.utils.ConstKt;
import storesOpenIn.com.utils.LiveLocationHelper;
import storesOpenIn.com.utils.LocationDecode;
import storesOpenIn.com.utils.PopupData;
import storesOpenIn.com.utils.PreferenceHelper;
import storesOpenIn.com.utils.RetrofitUtil;

import static storesOpenIn.com.utils.CommonExtensionsKt.fromJson;
import static storesOpenIn.com.utils.CommonExtensionsKt.isOnline;
import static storesOpenIn.com.utils.CommonExtensionsKt.shareApp;
import static storesOpenIn.com.utils.CommonExtensionsKt.showAlert;
import static storesOpenIn.com.utils.CommonExtensionsKt.showToast;
import static storesOpenIn.com.utils.ConstKt.KEY_LOCATION;
import static storesOpenIn.com.utils.ConstKt.KEY_STORE_DETAILS;

public class AddStoreActivity extends AppCompatActivity implements TextWatcher {

    private static final String TAG = "AddStoreActivity";
    @BindView(R.id.bottomsheet_map)
    View bottomSheet;
    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;
    @BindView(R.id.chipGroup)
    ChipGroup chipGroup;
    @BindView(R.id.editText_store_name)
    EditText etStoreName;
    @BindView(R.id.fabLocation)
    ImageView fabLocation;
    @BindView(R.id.editText_description)
    EditText etStoreDescription;
    @BindView(R.id.tv_street_address)
    TextView tvStreetAddress;
    @BindView(R.id.tv_placeholder_txt)
    TextView tvCurrentLocation;
    @BindView(R.id.tv_loc_name)
    TextView tvLocName;
    @BindView(R.id.btn_add_store)
    Button btnAddStore;
    @BindView(R.id.addStoreContainer)
    CoordinatorLayout addStoreContainer;
    @BindView(R.id.locationContainer)
    RelativeLayout locationContainer;
    @BindView(R.id.btnDeleteStore)
    MaterialButton btnDeleteStore;
    MarkerOptions markerOptions = new MarkerOptions();
    Marker marker;
    PreferenceHelper preferenceHelper;
    LiveLocationHelper liveLocationHelper;
    boolean isEditStore = false;
    private GoogleMap mGoogleMap;
    private LatLng mLatLng;
    private BottomSheetBehavior bsBehavior;
    private boolean chipClicked = false;
    private int selectedChipCount = 0;
    private LatLong currentLocation = new LatLong();
    private boolean categoryUpdated = false;
    private boolean storeUpdated = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        liveLocationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_store);
        isEditStore = (getIntent().getBooleanExtra(ConstKt.KEY_FROM_PROFILE, false));
        ButterKnife.bind(this);
        preferenceHelper = new PreferenceHelper(this);
        liveLocationHelper = new LiveLocationHelper(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_arrow));
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        setupChip();
        setUpBottomSheet();
        setDetailsIfEditStore();
        if (isEditStore) {
            toolbar.setTitle(getString(R.string.edit_store));
            btnAddStore.setText(R.string.update_store);
        }
        CommonExtensionsKt.setVisible(btnDeleteStore, isEditStore);
        etStoreName.addTextChangedListener(this);
        etStoreDescription.addTextChangedListener(this);
        liveLocationHelper.observe(this, new Observer<LatLong>() {
            @Override
            public void onChanged(LatLong latLong) {
                currentLocation = latLong;
                if (!isEditStore) {
                    setDetailsByLocation(currentLocation);
                    setUpMap(currentLocation);
                }
            }
        });


        fabLocation.setOnClickListener(v ->
        {
            setDetailsByLocation(currentLocation);
            mLatLng = new LatLng(currentLocation.getLat(), currentLocation.getLng());
            if (marker != null)
                marker.setPosition(mLatLng);
            if (mGoogleMap != null)
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
        });
        tvCurrentLocation.setOnClickListener(v -> {
            Intent intent = new Intent(AddStoreActivity.this, PlaceSelectionActivity.class);
            intent.putExtra(PlaceSelectionActivity.TITLE, getString(R.string.add_store));
            startActivityForResult(intent, 101);
        });
        locationContainer.setOnClickListener(v -> tvCurrentLocation.performClick());
        btnDeleteStore.setOnClickListener(v -> {
            if (isOnline(AddStoreActivity.this)) {
                deleteStoreAPICall();
            } else {
                showAlert(AddStoreActivity.this, null, getString(R.string.internet_check));
            }
        });

    }

    private void deleteStoreAPICall() {
        try {
            RequestBody requestBody = APIQuery.INSTANCE.deleteStore(((Store) getIntent().getParcelableExtra(KEY_STORE_DETAILS)).getId());
            Call<ResponseBody> responseBodyCall = RetrofitUtil.INSTANCE.getGraphQLAPI().jpostValue(
                    "",
                    "Bearer " + preferenceHelper.getUser_token_id(),
                    requestBody);
            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            DeleteResponse deleteResponse = fromJson(response.body().string(), DeleteResponse.class);
                            if (deleteResponse.getData().getDeleteStores().getAffectedRows() > 0) {
                                showToast(AddStoreActivity.this, getString(R.string.store_delete_success));
                                finish();
                            } else {
                                showToast(AddStoreActivity.this, getString(R.string.something_went_wrong));
                            }
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                        e.printStackTrace();
                        showToast(AddStoreActivity.this, getString(R.string.something_went_wrong));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            e.printStackTrace();
            showToast(AddStoreActivity.this, getString(R.string.something_went_wrong));
        }
    }

    private void setDetailsIfEditStore() {
        Store store = getIntent().getParcelableExtra(ConstKt.KEY_STORE_DETAILS);
        if (isEditStore) {
            Gson gson = new GsonBuilder().create();
            etStoreName.setText(store.getName());
            etStoreDescription.setText(store.getDescription());
            List<Double> coordinates = store.getLocationByLocation().getLocation().getCoordinates();
            try {
                LatLong latLong = new LatLong(coordinates.get(0), coordinates.get(1));
                setDetailsByLocation(latLong);
                setUpMap(latLong);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setDetailsByLocation(LatLng location) {
        setDetailsByLocation(new LatLong(location.latitude, location.longitude));
    }

    private void setDetailsByLocation(LatLong location) {
        String decodeGeo = LocationDecode.INSTANCE.decodeGeo(this, location);
        tvStreetAddress.setText(decodeGeo);
        String locality = LocationDecode.INSTANCE.getAddress(this, location).getLocality();
        if (locality == null)
            locality = "";
        tvLocName.setText(locality);
    }

    private void setupChip() {
        List<Item> categories = CommonExtensionsKt.getCategories(getResources().getStringArray(R.array.items));

        for (Item category : categories) {
            Chip chip = CommonExtensionsKt.addChip(chipGroup, category.getName().toUpperCase(), R.color.background_color_chip_state_list, R.style.ChipTextStyleSelected);

            if (isEditStore) {
                List<StoreXCategory> storeXCategories = ((Store) getIntent().getParcelableExtra(KEY_STORE_DETAILS)).getStoreXCategories();
                ListIterator<StoreXCategory> listIterator = storeXCategories.listIterator();
                while (listIterator.hasNext()) {
                    StoreXCategory next = listIterator.next();
                    Log.d(TAG, "setupChip: " + next.getCategory().getName());
                    if (next.getCategory().getId() == (category.getId())) {
                        listIterator.remove();
                        chip.setChecked(true);
                        break;
                    }
                }
            }

            chip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                checkEnableButton();
                if (isChecked) {
                    selectedChipCount = selectedChipCount + 1;
                } else {
                    if (selectedChipCount > 0) {
                        selectedChipCount = selectedChipCount - 1;
                    }
                }
            });
        }
    }


    private void setUpBottomSheet() {
        bsBehavior = BottomSheetBehavior.from(bottomSheet);
        bsBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

//        bsBehavior.setBottomSheetCallback(new AnchorSheetBehavior.AnchorSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                //Do Nothing
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//                }
//            }
//        });

        // set callback for changes
        bsBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @SuppressLint("SwitchIntDef")
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                float h = bottomSheet.getHeight();
                float off = h * slideOffset;
                switch (bsBehavior.getState()) {
                    case AnchorSheetBehavior.STATE_DRAGGING:
                    case AnchorSheetBehavior.STATE_SETTLING:
                        setMapPaddingBottom(off);
                        if (mLatLng != null)
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
                        break;
                    case AnchorSheetBehavior.STATE_HIDDEN:
                    case AnchorSheetBehavior.STATE_COLLAPSED:
                    case AnchorSheetBehavior.STATE_EXPANDED:
                    case AnchorSheetBehavior.STATE_ANCHOR:
                    case AnchorSheetBehavior.STATE_FORCE_HIDDEN:
                        break;
                }
            }
        });
    }

    private void setMapPaddingBottom(Float offset) {
        Float maxMapPaddingBottom = 1.0f;
        if (mGoogleMap != null)
            mGoogleMap.setPadding(0, 0, 0, Math.round(offset * maxMapPaddingBottom));
    }

    private void setUpMap(LatLong currentLocation) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> {
            googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    mLatLng = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
                    setDetailsByLocation(new LatLong(mLatLng.latitude, mLatLng.longitude));
                }
            });
            mGoogleMap = googleMap;
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);

            mLatLng = new LatLng(currentLocation.getLat(), currentLocation.getLng());

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
            markerOptions.draggable(true);
            markerOptions.position(mLatLng);
            markerOptions.title(
                    getString(R.string.long_press_to_move));
            marker = mGoogleMap.addMarker(markerOptions);
            marker.setTitle(getString(R.string.long_press_to_move));
            marker.showInfoWindow();
            mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    setDetailsByLocation(latLng);
                    marker.setPosition(latLng);
                    marker.setTitle(getString(R.string.long_press_to_move));
                    marker.showInfoWindow();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            });

        });
    }


    @OnClick(R.id.btn_add_store)
    void onAddStore() {
        if (isOnline(AddStoreActivity.this)) {
            if (!etStoreName.getText().toString().isEmpty()) {
                if (getIntent().getBooleanExtra(ConstKt.KEY_FROM_PROFILE, false)) {
                    updateStoreAPICall();
                } else {
                    checkAndAddStoreAPICall();
                }
            } else
                Toast.makeText(AddStoreActivity.this, R.string.enter_store_name, Toast.LENGTH_SHORT)
                        .show();

            CommonExtensionsKt.hideKeyBoard(AddStoreActivity.this, etStoreName);
        } else {
            showAlert(AddStoreActivity.this, null, getString(R.string.internet_check));
        }

    }

    private void checkAndAddStoreAPICall() {
        try {
            RequestBody requestBody = APIQuery.INSTANCE.checkStoreQuery(
                    etStoreName.getText().toString(),
                    mLatLng.latitude,
                    mLatLng.longitude);
            Call<ResponseBody> responseBodyCall = RetrofitUtil.INSTANCE.getGraphQLAPI().jpostValue(
                    "",
                    "Bearer " + preferenceHelper.getUser_token_id(),
                    requestBody);
            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        CheckStoreModel checkStoreModel = fromJson(response.body().string(), CheckStoreModel.class);
                        if (checkStoreModel.getData().getStores().size() > 0) {
                            showToast(AddStoreActivity.this, getString(R.string.store_already_added));
                        } else {
                            addStoreAPICall();
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                        e.printStackTrace();
                        showToast(AddStoreActivity.this, getString(R.string.something_went_wrong));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateStoreAPICall() {
        try {
            Store store = getIntent().getParcelableExtra(ConstKt.KEY_STORE_DETAILS);
            updateCategory(store);

            RetrofitUtil.GraphQL graphQLAPI = RetrofitUtil.INSTANCE.getGraphQLAPI();
            String token = "Bearer " + preferenceHelper.getUser_token_id();
            RequestBody locationQuery = APIQuery.INSTANCE.upsertLocationQuery(mLatLng.latitude, mLatLng.longitude);
            graphQLAPI.jpostValue("", token, locationQuery).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        UpsertLocationResponse upsertLocationResponse = fromJson(response.body().string(), UpsertLocationResponse.class);
                        long id = upsertLocationResponse.getData().getInsert_location().getReturning().get(0).getId();

                        graphQLAPI.jpostValue("", token, APIQuery.INSTANCE.updateStoreQuery(
                                etStoreName.getText().toString()
                                , etStoreDescription.getText().toString(),
                                store.getId(),
                                ((int) id))).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                storeUpdated = true;
                                if (categoryUpdated) {
                                    runOnUiThread(() -> showPopup(store.getId()));
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    FirebaseCrashlytics.getInstance().recordException(t);
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void updateCategory(Store store) {
        RequestBody insertCategoryQuery = APIQuery.INSTANCE.insertCategoryQuery(insertCategory(getCategoryIds(), store.getId()));
        RequestBody deleteCategoryQuery = APIQuery.INSTANCE.deleteCategoryQuery(store.getId());

        RetrofitUtil.GraphQL graphQLAPI = RetrofitUtil.INSTANCE.getGraphQLAPI();

        Call<ResponseBody> deleteCategoryCall = graphQLAPI.jpostValue(
                "",
                "Bearer " + preferenceHelper.getUser_token_id(),
                deleteCategoryQuery);
        Call<ResponseBody> insertCategoryCall = graphQLAPI.jpostValue(
                "",
                "Bearer " + preferenceHelper.getUser_token_id(),
                insertCategoryQuery);

        deleteCategoryCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    insertCategoryCall.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            categoryUpdated = true;
                            if (storeUpdated) {
                                runOnUiThread(() -> showPopup(store.getId()));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                Gson gson = new GsonBuilder().create();
                LatLongWrapper latLongWrapper = gson.fromJson(data.getStringExtra(KEY_LOCATION),
                        LatLongWrapper.class
                );
                LatLong location = new LatLong(latLongWrapper.getData().component1(), latLongWrapper.getData().component2());

                setDetailsByLocation(location);
                mLatLng = new LatLng(location.getLat(), location.getLng());
                if (marker != null)
                    marker.setPosition(mLatLng);
                if (mGoogleMap != null)
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addStoreAPICall() {
        try {
            RequestBody requestBody = APIQuery.INSTANCE.insertStoreQuery(
                    etStoreName.getText().toString(),
                    etStoreDescription.getText().toString(),
                    buildArray(getCategoryIds()),
                    mLatLng.latitude,
                    mLatLng.longitude);

            Call<ResponseBody> insertStore = RetrofitUtil.INSTANCE.getGraphQLAPI().jpostValue(
                    "",
                    "Bearer " + preferenceHelper.getUser_token_id(),
                    requestBody);
            insertStore.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        AddStoreData addStoreData = fromJson(response.body().string(), AddStoreData.class);
                        runOnUiThread(() -> {
                            List<Returning> storesList = addStoreData.getData().getInsertStores().getReturning();
                            if (!storesList.isEmpty()) {//Crashes here or returns an index out of bounds exception if no list is returned.
                                showPopup(addStoreData.getData().getInsertStores().getReturning().get(0).getId());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics.getInstance().recordException(e);
                        showToast(AddStoreActivity.this, "Something went wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    FirebaseCrashlytics.getInstance().recordException(t);
                }
            });
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            e.printStackTrace();
        }
    }

    private void showPopup(int storeID) {
        PopupData popupData = new PopupData(
                R.drawable.ic_store_add_success,
                getString(R.string.store_added_successfully),
                getString(R.string.store_added_successfully_descs),
                getString(R.string.view_store),
                getString(R.string.share_the_news),
                null);
        if (isEditStore) {
            popupData = new PopupData(
                    R.drawable.ic_store_add_success,
                    getString(R.string.store_updated_successfully),
                    "",
                    null,
                    getString(R.string.share_the_news),
                    null);
        }

        PopupWindow popupWindow = CommonExtensionsKt.showVolunteerPopup(addStoreContainer, popupData);
        popupWindow.getContentView().findViewById(R.id.btnSuccessAddAnother).setOnClickListener(v -> {
            Intent intent = new Intent(AddStoreActivity.this, StoreDetailsActivity.class);
            Store store = new Store(new Helpful(), storeID, "", new LocationByLocation(), "", new NotHelpful(), new ArrayList<>(), new ArrayList<>(), new User(), new ArrayList());
            intent.putExtra(StoreDetailsActivity.KEY_STORE_VALUE, store.toString());
            startActivity(intent);
        });
        popupWindow.getContentView().findViewById(R.id.btnSuccessShare).setOnClickListener(v -> {
            shareApp(AddStoreActivity.this, getString(R.string.storeShare) + storeID);
        });

    }

    private ArrayList<Integer> getCategoryIds() {
        ArrayList<Integer> categoryIDs = new ArrayList<>();
        for (int i = 0; i < chipGroup.getChildCount(); i++) {
            if (((Chip) chipGroup.getChildAt(i)).isChecked())
                categoryIDs.add(i + 1);
        }
        return categoryIDs;
    }

    private JSONArray buildArray(ArrayList<Integer> integers) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Integer integer : integers) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("category_id", integer);
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    private JSONArray insertCategory(ArrayList<Integer> integers, int storeId) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (Integer integer : integers) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("category_id", integer);
                jsonObject.put("is_active", true);
                jsonObject.put("store_id", storeId);
                jsonArray.put(jsonObject);
            }
            JsonObject jsonObject = new JsonObject();
            return jsonArray;
        } catch (Exception e) {
            return new JSONArray();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Do Nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Do Nothing
    }

    @Override
    public void afterTextChanged(Editable editable) {
        checkEnableButton();
    }

    private void checkEnableButton() {
        if (etStoreDescription.getText().length() >= 5 && etStoreName.getText().length() >= 5 && getCategoryIds().size() > 0)
            btnAddStore.setEnabled(true);
        else btnAddStore.setEnabled(false);
    }
}
